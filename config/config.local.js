/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-05 16:18:56
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-06-16 15:54:46
 * @FilePath: \garden_service\config\config.default.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1659687535068_6616';

  // add your middleware config here
  config.middleware = [];
  config.jwt = {
    secret: 'ea86dg645sdfg6sf',
    expiresIn: '24h',
  };

  // 跨域配置
  config.cors = {
    origin: '*',
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS',
    credentials: true, // Access-Control-Allow-Credentials 需指定为true
  };
  config.security = {
    csrf: {
      enable: false,
    },
    domainWhiteList: [ '*' ],
  };
  config.multipart = {
    mode: 'file',
  };


  // session
  config.session = {
    key: 'huarun',
    maxAge: 24 * 3600 * 1000 * 10000, // 1h
    httpOnly: true,
    encrypt: true,
    renew: true, // 当用户 Session 的有效期仅剩下最大有效期一半的时候，重置 Session 的有效期。
  };
  config.redis = { // 单个redis
    client: {
      port: 6379,
      host: '127.0.0.1',
      password: '',
      db: 0,
    },
    agent: true,
  };
  // Session的默认配置
  config.sessionRedis = {
    key: 'EGG_SESS',
    maxAge: 24 * 3600 * 1000 * 10000, // 1 天
    httpOnly: true,
    encrypt: false,
  };
  // add your user config here
  const userConfig = {
    //   // myAppName: 'egg',
    sequelize: {
      datasources: [
        {
          delegate: 'model', // 加载所有的模型到 app.model and ctx.model
          baseDir: 'model', // 要加载的模型目录`app/model/*.js`
          dialect: 'mysql', // support: mysql, mariadb, postgres, mssql
          database: 'huarun',
          host: 'localhost',
          port: 3306,
          username: 'root',
          password: '123456',
          define: {
            // 使用自定义的表名
            freezeTableName: true,
            // 自动生成时间戳 -小驼峰式
            timestamps: true,
            // 表名小驼峰
            underscored: false,
          },
          // 配置数据库时间为东八区北京时间
          timezone: '+08:00',
        }, { // 考勤
          delegate: 'attendanceMssql',
          baseDir: 'attendance_model',
          database: 'hwatt',
          dialect: 'mysql', // support: mysql, mariadb, postgres, mssql
          host: 'localhost',
          port: 3306,
          username: 'root',
          password: '123456',
          define: {
            // 使用自定义的表名
            freezeTableName: true,
            // 自动生成时间戳 -小驼峰式
            timestamps: true,
            // 表名小驼峰
            underscored: false,
          },
          // 配置数据库时间为东八区北京时间
          timezone: '+08:00',
        },
        { // 门禁
          delegate: 'accessControlModel', // 加载所有的模型到 app.model and ctx.model
          baseDir: 'accessControl_model', // 要加载的模型目录`app/model/*.js`
          dialect: 'mysql', // support: mysql, mariadb, postgres, mssql
          database: 'menjin',
          host: 'localhost',
          port: 3306,
          username: 'root',
          password: '123456',
          define: {
            // 使用自定义的表名
            freezeTableName: true,
            // 自动生成时间戳 -小驼峰式
            timestamps: true,
            // 表名小驼峰
            underscored: false,
          },
          // 配置数据库时间为东八区北京时间
          timezone: '+08:00',
        },
        { // 道闸
          delegate: 'mssql',
          baseDir: 'parked_model',
          database: 'parkedDB',
          dialect: 'mssql', // support: mysql, mariadb, postgres, mssql
          host: 'localhost',
          port: '1433',
          username: 'sa',
          password: '123456',
          define: {
            // 使用自定义的表名
            freezeTableName: true,
            // 自动生成时间戳 -小驼峰式
            timestamps: true,
            // 表名小驼峰
            underscored: false,
          },
          // 配置数据库时间为东八区北京时间
          timezone: '+08:00',
        },
        { // 温湿度及气象站
          delegate: 'environmentModel',
          baseDir: 'environment_model',
          database: 'huanjing',
          dialect: 'mssql', // support: mysql, mariadb, postgres, mssql
          host: 'localhost',
          port: '1433',
          username: 'sa',
          password: '123456',
          define: {
            // 使用自定义的表名
            freezeTableName: true,
            // 自动生成时间戳 -小驼峰式
            timestamps: true,
            // 表名小驼峰
            underscored: false,
          },
          // 配置数据库时间为东八区北京时间
          timezone: '+08:00',
        },
      ],
    },
    uploadAvatarDir: 'app/public/avatar/upload', // 上传头像路径
    uinoPath: 'http://10.0.0.23:1662',
    videoPath: 'http://127.0.0.1:8083/password123456',
    morningAttendanceRate: ' 11:00:00',
    afternoonAttendanceRate: '  12:30:00',
  };
  return {
    ...config,
    ...userConfig,
  };
};
