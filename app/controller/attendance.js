/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-27 11:06:13
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-09-08 13:13:21
 * @FilePath: \garden_service\app\controller\attendance.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const BaseController = require('./base');
const moment = require('moment');
const Op = require('sequelize').Op;
const _ = require('lodash');
class AttendanceController extends BaseController {
  async index() {
    const { ctx } = this;
    const data = {
      xData: [
        moment().subtract(3, 'days').format('DD') + '日',
        moment().subtract(2, 'days').format('DD') + '日',
        moment().subtract(1, 'days').format('DD') + '日',
        moment().format('DD') + '日',
      ],
      chartdata: {
        上午签到: [
          await ctx.service.attendance.getAttendanceCount(
            moment().subtract(3, 'days').format('YYYY-MM-DD'),
            moment().subtract(3, 'days').format('YYYY-MM-DD') +
              this.config.morningAttendanceRate
          ),
          await ctx.service.attendance.getAttendanceCount(
            moment().subtract(2, 'days').format('YYYY-MM-DD'),
            moment().subtract(2, 'days').format('YYYY-MM-DD') +
              this.config.morningAttendanceRate
          ),
          await ctx.service.attendance.getAttendanceCount(
            moment().subtract(1, 'days').format('YYYY-MM-DD'),
            moment().subtract(1, 'days').format('YYYY-MM-DD') +
              this.config.morningAttendanceRate
          ),
          await ctx.service.attendance.getAttendanceCount(
            moment().format('YYYY-MM-DD'),
            moment().format('YYYY-MM-DD') + this.config.morningAttendanceRate
          ),
        ],
        上午签退: [
          await ctx.service.attendance.getAttendanceCount(
            moment().subtract(3, 'days').format('YYYY-MM-DD') +
              this.config.morningAttendanceRate,
            moment().subtract(3, 'days').format('YYYY-MM-DD') +
              this.config.afternoonAttendanceRate
          ),
          await ctx.service.attendance.getAttendanceCount(
            moment().subtract(2, 'days').format('YYYY-MM-DD') +
              this.config.morningAttendanceRate,
            moment().subtract(2, 'days').format('YYYY-MM-DD') +
              this.config.afternoonAttendanceRate
          ),
          await ctx.service.attendance.getAttendanceCount(
            moment().subtract(1, 'days').format('YYYY-MM-DD') +
              this.config.morningAttendanceRate,
            moment().subtract(1, 'days').format('YYYY-MM-DD') +
              this.config.afternoonAttendanceRate
          ),
          await ctx.service.attendance.getAttendanceCount(
            moment().format('YYYY-MM-DD') + this.config.morningAttendanceRate,
            moment().format('YYYY-MM-DD') + this.config.afternoonAttendanceRate
          ),
        ],
        下午签到: [
          await ctx.service.attendance.getAttendanceCount(
            moment().subtract(3, 'days').format('YYYY-MM-DD') +
              this.config.afternoonAttendanceRate,
            moment().subtract(3, 'days').format('YYYY-MM-DD') + ' 15:00:00'
          ),
          await ctx.service.attendance.getAttendanceCount(
            moment().subtract(2, 'days').format('YYYY-MM-DD') +
              this.config.afternoonAttendanceRate,
            moment().subtract(2, 'days').format('YYYY-MM-DD') + ' 15:00:00'
          ),
          await ctx.service.attendance.getAttendanceCount(
            moment().subtract(1, 'days').format('YYYY-MM-DD') +
              this.config.afternoonAttendanceRate,
            moment().subtract(1, 'days').format('YYYY-MM-DD') + ' 15:00:00'
          ),
          await ctx.service.attendance.getAttendanceCount(
            moment().format('YYYY-MM-DD') + this.config.afternoonAttendanceRate,
            moment().format('YYYY-MM-DD') + ' 15:00:00'
          ),
        ],
        下午签退: [
          await ctx.service.attendance.getAttendanceCount(
            moment().subtract(3, 'days').format('YYYY-MM-DD') + ' 15:00:00',
            moment().subtract(3, 'days').format('YYYY-MM-DD') + ' 23:59:59'
          ),
          await ctx.service.attendance.getAttendanceCount(
            moment().subtract(2, 'days').format('YYYY-MM-DD') + ' 15:00:00',
            moment().subtract(2, 'days').format('YYYY-MM-DD') + ' 23:59:59'
          ),
          await ctx.service.attendance.getAttendanceCount(
            moment().subtract(1, 'days').format('YYYY-MM-DD') + ' 15:00:00',
            moment().subtract(1, 'days').format('YYYY-MM-DD') + ' 23:59:59'
          ),
          await ctx.service.attendance.getAttendanceCount(
            moment().format('YYYY-MM-DD') + ' 15:00:00',
            moment().format('YYYY-MM-DD') + ' 23:59:59'
          ),
        ],
      },
    };
    this.success(data);
  }
  /**
   * 人员考勤
   */
  async personnelAttendance() {
    const { ctx } = this;
    const options = {
      where: {
        CardTime: { [Op.gte]: moment().format('YYYY-MM-DD') + ' 00:00:00' },
      },
      include: [
        {
          model: ctx.attendanceMssql.Employee,
          as: 'employee',
          attributes: [ 'EmployeeName' ],
          include: [
            {
              model: ctx.attendanceMssql.Brch,
              as: 'brch',
              attributes: [ 'BrchName' ],
            },
          ],
        },
      ],
      attributes: [ 'CardID', 'EmployeeID', 'CardTime' ],
      order: [[ 'CardTime', 'DESC' ]],
      limit: 100,
    };
    const cardData = await ctx.attendanceMssql.Card.findAll(options);
    const attendanceData = {};
    attendanceData.titleName = [ '部门名称', '人员姓名', '打卡时间' ];
    const valueAry = [];
    cardData.forEach(element => {
      valueAry.push([
        element.employee.brch.BrchName,
        element.employee.EmployeeName,
        moment(element.CardTime).format('YYYY-MM-DD HH:mm:ss'),
      ]);
    });
    attendanceData.rowData = valueAry;
    this.success(attendanceData);
  }
  /**
   * 获取所有部门
   */
  async getBrch() {
    const { ctx } = this;
    const options = {
      where: { BrchID: { [Op.ne]: -2 }, [Op.and]: { BrchID: { [Op.ne]: 2 } } },
      attributes: [ 'BrchName', 'BrchID' ],
    };
    const data = await ctx.attendanceMssql.Brch.findAll(options);
    this.success(data);
  }
  /**
   * 考勤列表
   */
  async list() {
    const { ctx } = this;
    const { query } = ctx;
    if (_.has(query, 'page_no')) {
      query.page_no = parseInt(query.page_no);
    }
    if (_.has(query, 'page_size')) {
      query.page_size = parseInt(query.page_size);
    }
    const rules = {
      page_no: { type: 'int', required: false, min: 1 },
      page_size: { type: 'int', required: false, min: 1 },
    };
    try {
      ctx.validate(rules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(
        `${error.message}: ${error.errors[0].code} ${error.errors[0].field}`
      );
    }
    const brchOptions = {
      where: { BrchID: { [Op.ne]: -2 }, [Op.and]: { BrchID: { [Op.ne]: 2 } } },
      model: ctx.attendanceMssql.Brch,
      as: 'brch',
      attributes: [ 'BrchName', 'BrchID' ],
    };
    // 根据部门id查询
    if (_.has(query, 'BrchID') && !_.isEmpty(query.BrchID)) {
      brchOptions.where.BrchID = query.BrchID;
    }
    const employeeOptions = {
      where: {},
      model: ctx.attendanceMssql.Employee,
      as: 'employee',
      attributes: [ 'EmployeeName' ],
      include: [ brchOptions ],
    };
    // 根据人员姓名查询
    if (_.has(query, 'EmployeeName') && !_.isEmpty(query.EmployeeName)) {
      employeeOptions.where.EmployeeName = { [Op.like]: '%' + query.EmployeeName + '%' };
    }
    const page_no = query.page_no || 1;
    const options = {
      where: {},
      include: [ employeeOptions ],
      attributes: [ 'CardID', 'EmployeeID', 'CardTime' ],
      order: [[ 'CardTime', 'DESC' ]],
    };
    if (_.has(query, 'cardTimeStr') && !_.isEmpty(query.cardTimeStr)) {
      options.where.CardTime = { [Op.gte]: query.cardTimeStr, [Op.lte]: query.cardTimeEnd };
    }
    if (_.has(query, 'page_no') && _.has(query, 'page_size')) {
      options.offset = (query.page_no - 1) * query.page_size;
      options.limit = query.page_size;
    }
    const data = await ctx.attendanceMssql.Card.findAndCountAll(options);
    data.page_no = page_no;
    this.success(data);
  }
  /**
   * 部门考勤率
   */
  async departmentAttendanceRate() {
    const { ctx } = this;
    const currentDate = moment().format('YYYY-MM-DD HH:mm:ss');
    const cardOptions = {
      where: {},
      model: ctx.attendanceMssql.Card,
      as: 'card',
    };
    // 上午签到
    if (
      currentDate <=
      moment().format('YYYY-MM-DD') + this.config.morningAttendanceRate
    ) {
      cardOptions.where = {
        CardTime: {
          [Op.gte]: moment().format('YYYY-MM-DD') + ' 00:00:00',
          [Op.lte]:
            moment().format('YYYY-MM-DD') + this.config.morningAttendanceRate,
        },
      };
    } else {
      cardOptions.where = {
        CardTime: {
          [Op.gte]:
            moment().format('YYYY-MM-DD') + this.config.afternoonAttendanceRate,
          [Op.lte]: moment().format('YYYY-MM-DD') + ' 15:00:00',
        },
      };
    }
    const options = {
      where: { BrchID: { [Op.ne]: -2 }, [Op.and]: { BrchID: { [Op.ne]: 2 } } },
      include: [
        {
          model: ctx.attendanceMssql.Employee,
          as: 'employee',
          attributes: [
            'EmployeeID',
            'BrchID',
            [
              ctx.attendanceMssql.Sequelize.literal(`(
          SELECT COUNT(*)
          FROM kqz_employee AS employee
          WHERE
            employee.BrchID = kqz_brch.BrchID
      )`),
              'employee_count',
            ],
          ],
          include: [ cardOptions ],
        },
      ],
      attributes: [ 'BrchName', 'BrchID' ],
    };
    const personnelTotalAmount = await ctx.attendanceMssql.Brch.findAll(options);
    const data = [];

    personnelTotalAmount.forEach((element, index) => {
      data.push([
        index,
        element.BrchName,
        element.employee.length > 0
          ? (
            Number(
              element.employee.length /
                  element.employee[0]?.dataValues.employee_count
            ) * 100
          ).toFixed(2)
          : 0,
      ]);
    });
    const sortData = data.sort((a, b) => {
      return b[2] - a[2];
    });
    sortData.forEach((item, index) => {
      item[0] = index + 1;
    });
    this.success(sortData);
  }
  /**
   * 考勤率
   */
  async getAttendanceRate() {
    const { ctx } = this;
    const personnelTotalAmount =
    await ctx.attendanceMssql.Employee.findAndCountAll({
      where: {
        BrchID: { [Op.ne]: -2 },
        [Op.and]: { BrchID: { [Op.ne]: 2 } },
      },
    });
    let personnelCount;
    if (
      moment().format('YYYY-MM-DD HH:mm:ss') <
      moment().format('YYYY-MM-DD') + this.config.morningAttendanceRate
    ) {
      personnelCount = await ctx.service.attendance.getAttendanceCount(
        moment().format('YYYY-MM-DD'),
        moment().format('YYYY-MM-DD') + this.config.morningAttendanceRate
      );
    } else {
      personnelCount = await ctx.service.attendance.getAttendanceCount(
        moment().format('YYYY-MM-DD') + this.config.afternoonAttendanceRate,
        moment().format('YYYY-MM-DD') + ' 15:00:00'
      );
    }
    const data = [
      {
        key: '已签到',
        value: personnelCount || 0,
      },
      {
        key: '未签到',
        value: personnelTotalAmount.count - (personnelCount || 0),
      },
    ];
    this.success(data);
  }
}

module.exports = AttendanceController;
