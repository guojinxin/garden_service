/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-01 17:29:04
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-04-21 09:53:49
 * @FilePath: \garden_service\app\controller\alarm.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const BaseController = require('./base');
const Op = require('sequelize').Op;
const _ = require('lodash');
const moment = require('moment');

class YAlarmController extends BaseController {
  /**
   * 告警列表
   */
  async index() {
    const { ctx } = this;
    const { query } = ctx;
    if (_.has(query, 'page_no')) {
      query.page_no = parseInt(query.page_no);
    }
    if (_.has(query, 'page_size')) {
      query.page_size = parseInt(query.page_size);
    }
    const rules = {
      page_no: { type: 'int', required: false, min: 1 },
      page_size: { type: 'int', required: false, min: 1 },
      ciName: { type: 'string', required: false, allowEmpty: true },
      alarmlevel: { type: 'string', required: false, allowEmpty: true },
      status: { type: 'string', required: false, allowEmpty: true },
    };
    try {
      ctx.validate(rules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    const page_no = query.page_no || 1;
    const options = {
      where: {},
      include: [{ model: ctx.model.YAdminUser, as: 'adminUser', attributes: [ 'id', 'name', 'gender', 'phone', 'jobNum', 'department', 'isApproval', 'email' ], required: false }],
      order: [[ 'alarm_time', 'DESC' ]],
    };
    if (_.has(query, 'ciName') && !_.isEmpty(query.ciName)) {
      options.where.ciName = { [Op.like]: '%' + query.ciName + '%' };
    }
    if (_.has(query, 'alarmlevel') && !_.isEmpty(query.alarmlevel)) {
      options.where.alarmlevel = query.alarmlevel;
    }
    if (_.has(query, 'status') && !_.isEmpty(query.status)) {
      options.where.status = query.status;
    }
    if (_.has(query, 'page_no') && _.has(query, 'page_size')) {
      options.offset = (query.page_no - 1) * query.page_size;
      options.limit = query.page_size;
    }
    const userStore = ctx.session.app_user;

    if (userStore.role !== '2') {
      options.where.distributionUser_Id = userStore.id;
    }
    const data = await ctx.model.YAlarm.findAndCountAll(options);
    data.page_no = page_no;
    this.success(data);
  }
  async handle() {
    const { ctx, config } = this;
    const gatepost = JSON.parse(ctx.request.body.data)[0].gatepost;
    await this.ctx.curl(`${config.uinoPath + '/thing/dix/monitor/alarm'}`, { method: 'POST', data: [
      {
        cikey: gatepost.ciName,
        severity: gatepost._ALARMLEVEL_,
        kpiname: 'gatepost',
        status: 2,
        lasttime: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
      },
    ], contentType: 'json',
    dataType: 'json' });

    this.success('成功');
  }
  async updateStatus() {
    const { ctx, config } = this;
    const { params } = ctx;
    const { body } = ctx.request;
    const paramRules = {
      id: { type: 'string', required: true, allowEmpty: false },
    };
    try {
      ctx.validate(paramRules, params);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    const alarData = await ctx.model.YAlarm.findOne({ where: { id: params.id } });
    if (_.isEmpty(alarData)) {
      return this.badRequest('不存在该报警');
    }
    if (body.status === '3') {
      await this.ctx.curl(`${config.uinoPath + '/thing/dix/monitor/alarm'}`, { method: 'POST', data: [
        {
          cikey: alarData.ciName,
          severity: alarData.alarmlevel,
          kpiname: 'gatepost',
          status: 2,
          lasttime: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        },
      ], contentType: 'json',
      dataType: 'json' });
    }
    body.modify_time = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
    const data = await ctx.model.YAlarm.update(body, { where: { id: params.id } });
    this.success(data);
  }
  async sendAlarm() {
    const { ctx } = this;
    const { body } = ctx.request;
    await ctx.service.alarm.sendAlarm(body.params);
    this.success('成功');
  }
  // 获取告警统计列表
  async getAlarmStatistics() {
    // const { ctx } = this;
    // const { alarm } = ctx.service;
    const alarmData = await this.ctx.model.YAlarm.findAll({
      group: [ 'alarmType' ],
      attributes: [ 'alarmType', [ this.ctx.attendanceMssql.Sequelize.fn('COUNT', 'alarmType'), 'alarm_count' ]],
    });
    // 总数
    let alarmCount = 0;
    alarmData.forEach(item => {
      alarmCount += item.dataValues.alarm_count;
    });
    // 消防告警数量
    const fireAlarmCount = _.find(alarmData, item => { return item.dataValues.alarmType === '4'; })?.dataValues.alarm_count || 0;
    // 温湿度告警数量
    const humitureAlarmCount = _.find(alarmData, item => { return item.dataValues.alarmType === '3'; })?.dataValues.alarm_count || 0;
    // 门禁告警数量
    const sewageAlarmCount = _.find(alarmData, item => { return item.dataValues.alarmType === '7'; })?.dataValues.alarm_count || 0;
    const monitorAlarmCount = _.find(alarmData, item => { return item.dataValues.alarmType === '8'; })?.dataValues.alarm_count || 0;
    // 其他告警的数量
    const otherAlarmCount = alarmCount - fireAlarmCount - humitureAlarmCount - sewageAlarmCount;
    const data = [
      fireAlarmCount !== 0 ? Number(((fireAlarmCount / alarmCount) * 100).toFixed(2)) : 0,
      humitureAlarmCount !== 0 ? Number(((humitureAlarmCount / alarmCount) * 100).toFixed(2)) : 0,
      sewageAlarmCount !== 0 ? Number(((sewageAlarmCount / alarmCount) * 100).toFixed(2)) : 0,
      monitorAlarmCount !== 0 ? Number(((monitorAlarmCount / alarmCount) * 100).toFixed(2)) : 0,
      otherAlarmCount !== 0 ? Number(((otherAlarmCount / alarmCount) * 100).toFixed(2)) : 0,
    ];
    this.success(data);
  }
  // 过去6个月的告警
  async getAlarmDate() {
    const { ctx } = this;
    const { alarm } = ctx.service;
    const data = {
      yDataLeft: [
        moment().subtract(5, 'months').format('MM') + '月',
        moment().subtract(4, 'months').format('MM') + '月',
        moment().subtract(3, 'months').format('MM') + '月',
      ],
      yDataRight: [
        moment().subtract(2, 'months').format('MM') + '月',
        moment().subtract(1, 'months').format('MM') + '月',
        moment().format('MM') + '月',
      ],
      data: {
        leftChartValue: [
          await alarm.getAlarm(moment().subtract(5, 'months').format('YYYY-MM')),
          await alarm.getAlarm(moment().subtract(4, 'months').format('YYYY-MM')),
          await alarm.getAlarm(moment().subtract(3, 'months').format('YYYY-MM')),
        ],
        rightChartValue: [
          await alarm.getAlarm(moment().subtract(2, 'months').format('YYYY-MM')),
          await alarm.getAlarm(moment().subtract(1, 'months').format('YYYY-MM')),
          await alarm.getAlarm(moment().format('YYYY-MM')),
        ],
      },
    };
    this.success(data);

  }
}
module.exports = YAlarmController;
