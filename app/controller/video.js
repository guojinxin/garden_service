/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-09-09 09:42:40
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-12-01 10:31:04
 * @FilePath: \garden_service\app\controller\visitState.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const BaseController = require('./base');
const { exec } = require('child_process');

class YVisitStateController extends BaseController {
  /**
   * 视频转码
   */
  async index() {
    const { ctx } = this;
    const { query } = ctx;
    const rules = {
      id: { type: 'string', required: true, allowEmpty: false },
    };
    try {
      ctx.validate(rules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    try {
      // `ffmpeg -rtsp_transport tcp  -i "rtsp://admin:admin123456@172.168.2.10:554/cam/realmonitor?channel=${query.id}&subtype=1" -q 0 -f mpegts -codec:v mpeg1video -s 1366*768 ${this.config.videoPath}`
      exec(`ffmpeg -rtsp_transport tcp  -i "rtsp://admin:admin123456@172.168.2.10:554/cam/realmonitor?channel=${query.id}&subtype=1" -q 0 -f mpegts -codec:v mpeg1video -s 1366*768 ${this.config.videoPath}`, function(err, stdout) {
        if (!err) {
          // 成功
          console.log(stdout);
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

}

module.exports = YVisitStateController;
