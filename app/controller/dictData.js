/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-01 17:29:04
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-03-22 09:43:54
 * @FilePath: \garden_service\app\controller\alarm.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const BaseController = require('./base');
const Op = require('sequelize').Op;
const _ = require('lodash');
// const moment = require('moment');
class YDictionaryController extends BaseController {
  async create() {
    const { ctx } = this;
    const { body } = ctx.request;
    const rules = {
      name: { type: 'string', required: true, allowEmpty: false },
      type: { type: 'string', required: true, allowEmpty: false },
      position: { type: 'string', required: true, allowEmpty: false },
    };
    try {
      ctx.validate(rules, body);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    const dictionaryData = await ctx.model.YDictionaryData.findOne({ where: { name: body.name } });
    if (!_.isEmpty(dictionaryData)) {
      return this.badRequest('当前名称重复不允许重复添加');
    }
    const userStore = ctx.session.app_user;

    if (userStore.role === '2') {
      const data = await ctx.model.YDictionaryData.create(body);
      this.success(data);
    } else {
      this.unauthorized('此服务只能管理员使用');
    }
  }
  async update() {
    const { ctx } = this;
    const { params } = ctx;
    const { body } = ctx.request;
    const paramRules = {
      id: { type: 'string', required: true, allowEmpty: false },
    };
    try {
      ctx.validate(paramRules, params);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    const userStore = ctx.session.app_user;
    if (userStore.role === '2') {
      const data = await ctx.model.YDictionaryData.update(body, { where: { id: params.id } });
      this.success(data);
    } else {
      this.unauthorized('此服务只能管理员使用');
    }
  }
  async index() {
    const { ctx } = this;
    const { query } = ctx;
    if (_.has(query, 'page_no')) {
      query.page_no = parseInt(query.page_no);
    }
    if (_.has(query, 'page_size')) {
      query.page_size = parseInt(query.page_size);
    }
    const rules = {
      page_no: { type: 'int', required: false, min: 1 },
      page_size: { type: 'int', required: false, min: 1 },
      name: { type: 'string', required: false, allowEmpty: true },
      type: { type: 'string', required: false, allowEmpty: true },
      status: { type: 'string', required: false, allowEmpty: true },
    };
    try {
      ctx.validate(rules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    const page_no = query.page_no || 1;
    const options = {
      where: {},
      order: [[ 'status', 'DESC' ], [ 'created_time', 'DESC' ]],
    };
    if (_.has(query, 'name') && !_.isEmpty(query.name)) {
      options.where.name = { [Op.like]: '%' + query.name + '%' };
    }
    if (_.has(query, 'type') && !_.isEmpty(query.type)) {
      options.where.type = query.type;
    }
    if (_.has(query, 'status') && !_.isEmpty(query.status)) {
      options.where.status = query.status;
    }
    if (_.has(query, 'page_no') && _.has(query, 'page_size')) {
      options.offset = (query.page_no - 1) * query.page_size;
      options.limit = query.page_size;
    }
    const data = await ctx.model.YDictionaryData.findAndCountAll(options);
    data.page_no = page_no;
    this.success(data);
  }
}

module.exports = YDictionaryController;
