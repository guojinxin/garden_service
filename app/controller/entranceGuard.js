'use strict';
const moment = require('moment');
const BaseController = require('./base');
const Op = require('sequelize').Op;
const _ = require('lodash');
class EntranceGuardController extends BaseController {
  /**
   * 门禁轮播列表
   */
  async index() {
    const { ctx } = this;
    const options = {
      where: {
        CreateDateTime: {
          [Op.gte]: moment().format('YYYY-MM-DD') + ' 00:00:00',
          [Op.lte]: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        },
        PersonnelID: { [Op.ne]: 0 },
      },
      attributes: [
        'ID',
        'Name',
        'PersonnelID',
        'CreateDateTime',
        'DevLocation',
      ],
      order: [[ 'CreateDateTime', 'DESC' ]],
    };
    const swingCardData = await ctx.accessControlModel.SwingCard.findAll(
      options
    );
    const data = [];
    swingCardData.forEach(element => {
      data.push({
        name: element.dataValues.Name,
        from: element.dataValues.DevLocation,
        time: moment(element.dataValues.CreateDateTime).format('HH:mm:ss'),
        type: '入',
        imgurl: '',
      });
    });

    this.success(data);
  }
  async personnel() {
    const { ctx } = this;
    const personnelTotalAmount =
      await ctx.attendanceMssql.Employee.findAndCountAll({
        where: {
          BrchID: { [Op.ne]: -2 },
          [Op.and]: { BrchID: { [Op.ne]: 2 } },
        },
      });
    const personnelMorning = await ctx.service.attendance.getAttendanceCount(
      moment().format('YYYY-MM-DD'),
      moment().format('YYYY-MM-DD') + this.config.morningAttendanceRate
    );
    const personnelAfternoon = await ctx.service.attendance.getAttendanceCount(
      moment().format('YYYY-MM-DD') + this.config.afternoonAttendanceRate,
      moment().format('YYYY-MM-DD') + ' 15:00:00'
    );
    this.success({
      数量统计: [
        ((personnelMorning / personnelTotalAmount.count) * 100).toFixed(1) ||
          '0',
        ((personnelAfternoon / personnelTotalAmount.count) * 100).toFixed(1) ||
          '0',
        await ctx.service.parked.getVisitUserCount(),
        await ctx.service.parked.getParkedCount(),
        await ctx.service.parked.getOutParkedCount(),
      ],
    });
  }
  // 查询门禁的部门
  async accessControlDepartment() {
    const { ctx } = this;
    const options = {
      where: { TypeID: 6 },
      attributes: [ 'ID', 'TypeID', 'Name' ],
    };
    const data = await ctx.accessControlModel.DepartmentInfo.findAll(options);
    this.success(data);
  }
  // 查看巡更点位
  async getPoint() {
    const { ctx } = this;
    const data = await ctx.model.YPoint.findAll({});
    this.success(data);
  }
  async getPatrolDataList() {
    const { ctx } = this;
    const { query } = ctx;
    if (_.has(query, 'page_no')) {
      query.page_no = parseInt(query.page_no);
    }
    if (_.has(query, 'page_size')) {
      query.page_size = parseInt(query.page_size);
    }
    const rules = {
      page_no: { type: 'int', required: false, min: 1 },
      page_size: { type: 'int', required: false, min: 1 },
    };
    try {
      ctx.validate(rules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(
        `${error.message}: ${error.errors[0].code} ${error.errors[0].field}`
      );
    }
    const page_no = query.page_no || 1;
    const options = {
      where: {},
      attributes: [ 'checkpointName', 'patrolmanName', 'createTime' ],
      order: [[ 'createTime', 'DESC' ]],
    };
    if (_.has(query, 'createTimeStr') && !_.isEmpty(query.createTimeStr)) {
      options.where.createTime = {
        [Op.gte]: query.createTimeStr,
        [Op.lte]: query.createTimeEnd,
      };
    }
    if (_.has(query, 'patrolmanName') && !_.isEmpty(query.patrolmanName)) {
      options.where.patrolmanName = {
        [Op.like]: '%' + query.patrolmanName + '%',
      };
    }
    if (_.has(query, 'checkpointName') && !_.isEmpty(query.checkpointName)) {
      options.where.checkpointName = query.checkpointName;
    }
    if (_.has(query, 'page_no') && _.has(query, 'page_size')) {
      options.offset = (query.page_no - 1) * query.page_size;
      options.limit = query.page_size;
    }
    const data = await ctx.model.YPatrol.findAndCountAll(options);
    data.page_no = page_no;
    this.success(data);
  }
  // 获取门禁列表
  async getDepartmentList() {
    const { ctx } = this;
    const { query } = ctx;
    if (_.has(query, 'page_no')) {
      query.page_no = parseInt(query.page_no);
    }
    if (_.has(query, 'page_size')) {
      query.page_size = parseInt(query.page_size);
    }
    const rules = {
      page_no: { type: 'int', required: false, min: 1 },
      page_size: { type: 'int', required: false, min: 1 },
    };
    try {
      ctx.validate(rules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(
        `${error.message}: ${error.errors[0].code} ${error.errors[0].field}`
      );
    }
    const departmentOption = {
      where: { TypeID: 6 },
      attributes: [ 'ID', 'TypeID', 'Name' ],
      model: ctx.accessControlModel.DepartmentInfo,
      as: 'objectinfo',
      required: true,
    };
    if (_.has(query, 'departmentName') && !_.isEmpty(query.departmentName)) {
      departmentOption.where.Name = query.departmentName;
    }
    const personnelOptions = {
      where: {},
      attributes: [ 'ParentID', 'Name' ],
      model: ctx.accessControlModel.Personnel,
      as: 'personnel',
      include: [ departmentOption ],
      required: true,
    };
    if (_.has(query, 'Name') && !_.isEmpty(query.Name)) {
      personnelOptions.where.Name = { [Op.like]: '%' + query.Name + '%' };
    }
    const options = {
      where: {},
      attributes: [ 'PersonnelID', 'CreateDateTime', 'DevLocation' ],
      include: [ personnelOptions ],
      order: [[ 'CreateDateTime', 'DESC' ]],
    };
    // 根据设备位置
    if (_.has(query, 'DevLocation') && !_.isEmpty(query.DevLocation)) {
      options.where.DevLocation = { [Op.like]: '%' + query.DevLocation + '%' };
    }
    if (_.has(query, 'createTimeStr') && !_.isEmpty(query.createTimeStr)) {
      options.where.CreateDateTime = {
        [Op.gte]: query.createTimeStr,
        [Op.lte]: query.createTimeEnd,
      };
    }
    if (_.has(query, 'page_no') && _.has(query, 'page_size')) {
      options.offset = (query.page_no - 1) * query.page_size;
      options.limit = query.page_size;
    }
    const data = await ctx.accessControlModel.SwingCard.findAndCountAll(options);
    this.success(data);
  }
  // 获取巡更列表
  async getPatrolList() {
    const { ctx } = this;
    const patrolData = await ctx.model.YPatrol.findAll({});
    const valueAry = [];
    patrolData.forEach(element => {
      const data = {};
      data.checkpointName = element.dataValues.checkpointName;
      data.patrolmanName = element.dataValues.patrolmanName;
      data.createTime = moment(element.dataValues.createTime).format('HH:mm');
      valueAry.push(Object.values(data));
    });
    this.success(valueAry);
  }
  async getPatrol() {
    const { ctx } = this;
    const { body } = ctx.request;
    const patrolData = await ctx.model.YPatrol.findAll({
      where: { checkpointId: body.ciCode },
    });
    const swingCardData = {};
    swingCardData.titleName = [ '序号', '巡检员名称', '打卡时间' ];
    const valueAry = [];
    patrolData.forEach((element, index) => {
      const data = {};
      data.index = index + 1;
      data.patrolmanName = element.dataValues.patrolmanName;
      data.createTime = moment(element.dataValues.createTime).format('HH:mm');
      valueAry.push(Object.values(data));
    });
    swingCardData.rowData = valueAry;
    this.success(swingCardData);
  }
  async getEntranceGuardIndex() {
    const { ctx } = this;
    const { body } = ctx.request;

    const netDeviceData = await ctx.accessControlModel.NetDevice.findOne({
      where: { IP: body.ciCode },
    });
    if (_.isEmpty(netDeviceData)) {
      return this.badRequest('该设备不存在');
    }
    const options = {
      where: {
        CreateDateTime: {
          [Op.gte]: moment().format('YYYY-MM-DD') + ' 00:00:00',
          [Op.lte]: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        },
        DeviceID: netDeviceData.ID,
      },
      attributes: [
        'ID',
        'PersonnelID',
        'CreateDateTime',
        'DevLocation',
        'DeviceID',
      ],
      include: [
        {
          model: ctx.accessControlModel.Personnel,
          as: 'personnel',
          attributes: [ 'Name' ],
        },
      ],
      order: [[ 'CreateDateTime', 'DESC' ]],
    };
    const swingCardList = await ctx.accessControlModel.SwingCard.findAll(
      options
    );
    const swingCardData = {};
    swingCardData.titleName = [ '序号', '姓名', '进出时间' ];
    const valueAry = [];
    swingCardList.forEach((element, index) => {
      const data = {};
      data.index = index + 1;
      data.name = element.dataValues.personnel?.Name;
      data.CreateDateTime = moment(element.dataValues.CreateDateTime).format(
        'HH:mm'
      );
      valueAry.push(Object.values(data));
    });
    swingCardData.rowData = valueAry;
    this.success(swingCardData);
  }
}

module.exports = EntranceGuardController;
