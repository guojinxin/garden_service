/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-05 17:03:12
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-05-16 13:49:54
 * @FilePath: \garden_service\app\controller\useWater.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const BaseController = require('./base');
const Op = require('sequelize').Op;
const _ = require('lodash');
const moment = require('moment');
class YUseWaterController extends BaseController {
  // 查询数字字典
  async getDictionaryData() {
    const { ctx } = this;
    const { query } = ctx;
    const rules = {
      type: { type: 'string', required: false, allowEmpty: true },
    };
    try {
      ctx.validate(rules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    const options = {
      where: { status: '1', type: query.type },
      order: [[ 'created_time', 'DESC' ]],
    };
    const data = await ctx.model.YDictionaryData.findAll(options);
    this.success(data);
  }
  async index() {
    const { ctx } = this;
    const { query } = ctx;
    if (_.has(query, 'page_no')) {
      query.page_no = parseInt(query.page_no);
    }
    if (_.has(query, 'page_size')) {
      query.page_size = parseInt(query.page_size);
    }
    const rules = {
      page_no: { type: 'int', required: false, min: 1 },
      page_size: { type: 'int', required: false, min: 1 },
      name: { type: 'string', required: false, allowEmpty: true },
      entryTimeStr: { type: 'string', required: false, allowEmpty: true },
      entryTimeEnd: { type: 'string', required: false, allowEmpty: true },
    };
    try {
      ctx.validate(rules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    const page_no = query.page_no || 1;
    const options = {
      where: { isDelete: false },
      order: [[ 'created_time', 'DESC' ]],
    };
    if (_.has(query, 'name') && !_.isEmpty(query.name)) {
      options.where.name = { [Op.like]: '%' + query.name + '%' };
    }
    if (_.has(query, 'entryTimeStr') && !_.isEmpty(query.entryTimeStr)) {
      options.where.entry_time = { [Op.gte]: moment(query.entryTimeStr).format('YYYY-MM'), [Op.lte]: moment(query.entryTimeEnd).format('YYYY-MM') };
    }
    if (_.has(query, 'page_no') && _.has(query, 'page_size')) {
      options.offset = (query.page_no - 1) * query.page_size;
      options.limit = query.page_size;
    }
    console.log(options);
    const data = await ctx.model.YUseWater.findAndCountAll(options);
    data.page_no = page_no;
    this.success(data);
  }
  async waterStatistics() {
    const { ctx } = this;
    const { energy } = ctx.service;
    const data = {
      去年: [
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-01', '0'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-02', '0'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-03', '0'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-04', '0'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-05', '0'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-06', '0'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-07', '0'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-08', '0'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-09', '0'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-10', '0'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-11', '0'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-12', '0'),
      ],
      今年: [
        await energy.getWaterLastYearData(moment().format('YYYY') + '-01', '0'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-02', '0'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-03', '0'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-04', '0'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-05', '0'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-06', '0'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-07', '0'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-08', '0'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-09', '0'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-10', '0'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-11', '0'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-12', '0'),
      ],
    };
    this.success(data);
  }
  async getAutomaticWater() {
    const waterOne = await this.ctx.curl('http://120.25.227.96:24201/iotserver/v1/lastlog', {
      method: 'POST',
      headers: {
        appKey: '9b58bf9421a241c23e889d83e43ca4b2',
      },
      data: {
        meterNo: '2206200309',
      },
      contentType: 'json',
      dataType: 'json',
    }
    );
    const waterTwo = await this.ctx.curl('http://120.25.227.96:24201/iotserver/v1/lastlog', {
      method: 'POST',
      headers: {
        appKey: '9b58bf9421a241c23e889d83e43ca4b2',
      },
      data: {
        meterNo: '2206200400',
      },
      contentType: 'json',
      dataType: 'json',
    }
    );
    const data = {
      key1: { content: waterOne.data.data[0].meterJbs },
      key2: { content: waterTwo.data.data[0].meterJbs },
    };
    this.success(data);
  }
  async getEnergyAll() {
    const waterData = await this.ctx.model.YTotalEnergy.findAll({ where: { created_time: { [Op.gte]: moment().format('YYYY') }, type: '0' }, attributes: [ 'total' ] });
    const electricityData = await this.ctx.model.YTotalEnergy.findAll({ where: { created_time: { [Op.gte]: moment().format('YYYY') }, type: '1' }, attributes: [ 'total' ] });
    const steamData = await this.ctx.model.YTotalEnergy.findAll({ where: { created_time: { [Op.gte]: moment().format('YYYY') }, type: '2' }, attributes: [ 'total' ] });

    let electricityTotal = 0;
    let waterTotal = 0;
    let steamTotal = 0;

    waterData.forEach(item => {
      waterTotal = waterTotal + item.total;
    });
    electricityData.forEach(item => {
      electricityTotal = electricityTotal + item.total;
    });
    steamData.forEach(item => {
      steamTotal = steamTotal + item.total;
    });
    const data = [
      String(waterTotal.toFixed(2)).replace(/\B(?=(\d{3})+(?!\d))/g, ','),
      String(steamTotal.toFixed(2)).replace(/\B(?=(\d{3})+(?!\d))/g, ','),
      String(electricityTotal.toFixed(2)).replace(/\B(?=(\d{3})+(?!\d))/g, ','),
    ];
    this.success(data);
  }
  async getEnergyLine() {
    const { ctx } = this;
    const { energy } = ctx.service;
    const data = {
      chartdata: {
        '水量(百吨)': [
          await energy.getWaterTotalLastYearData(moment().subtract(6, 'months').format('YYYY-MM'), '0'),
          await energy.getWaterTotalLastYearData(moment().subtract(5, 'months').format('YYYY-MM'), '0'),
          await energy.getWaterTotalLastYearData(moment().subtract(4, 'months').format('YYYY-MM'), '0'),
          await energy.getWaterTotalLastYearData(moment().subtract(3, 'months').format('YYYY-MM'), '0'),
          await energy.getWaterTotalLastYearData(moment().subtract(2, 'months').format('YYYY-MM'), '0'),
          await energy.getWaterTotalLastYearData(moment().subtract(1, 'months').format('YYYY-MM'), '0'),
        ],
        '电量(千千瓦时)': [
          await energy.getWaterTotalLastYearData(moment().subtract(6, 'months').format('YYYY-MM'), '1'),
          await energy.getWaterTotalLastYearData(moment().subtract(5, 'months').format('YYYY-MM'), '1'),
          await energy.getWaterTotalLastYearData(moment().subtract(4, 'months').format('YYYY-MM'), '1'),
          await energy.getWaterTotalLastYearData(moment().subtract(3, 'months').format('YYYY-MM'), '1'),
          await energy.getWaterTotalLastYearData(moment().subtract(2, 'months').format('YYYY-MM'), '1'),
          await energy.getWaterTotalLastYearData(moment().subtract(1, 'months').format('YYYY-MM'), '1'),
        ],
        '蒸汽(百吨)': [
          await energy.getWaterTotalLastYearData(moment().subtract(6, 'months').format('YYYY-MM'), '2'),
          await energy.getWaterTotalLastYearData(moment().subtract(5, 'months').format('YYYY-MM'), '2'),
          await energy.getWaterTotalLastYearData(moment().subtract(4, 'months').format('YYYY-MM'), '2'),
          await energy.getWaterTotalLastYearData(moment().subtract(3, 'months').format('YYYY-MM'), '2'),
          await energy.getWaterTotalLastYearData(moment().subtract(2, 'months').format('YYYY-MM'), '2'),
          await energy.getWaterTotalLastYearData(moment().subtract(1, 'months').format('YYYY-MM'), '2'),
        ],
      },
      xData: [
        moment().subtract(6, 'months').format('MM') + '月',
        moment().subtract(5, 'months').format('MM') + '月',
        moment().subtract(4, 'months').format('MM') + '月',
        moment().subtract(3, 'months').format('MM') + '月',
        moment().subtract(2, 'months').format('MM') + '月',
        moment().subtract(1, 'months').format('MM') + '月',
      ],
    };
    this.success(data);
  }
  async getAmountEnergyLine() {
    const { ctx } = this;
    const { energy } = ctx.service;
    const data = {
      chartdata: {
        水量: [
          await energy.getAmountYearData(moment().subtract(6, 'months').format('YYYY-MM'), '0'),
          await energy.getAmountYearData(moment().subtract(5, 'months').format('YYYY-MM'), '0'),
          await energy.getAmountYearData(moment().subtract(4, 'months').format('YYYY-MM'), '0'),
          await energy.getAmountYearData(moment().subtract(3, 'months').format('YYYY-MM'), '0'),
          await energy.getAmountYearData(moment().subtract(2, 'months').format('YYYY-MM'), '0'),
          await energy.getAmountYearData(moment().subtract(1, 'months').format('YYYY-MM'), '0'),
        ],
        电量: [
          await energy.getAmountYearData(moment().subtract(6, 'months').format('YYYY-MM'), '1'),
          await energy.getAmountYearData(moment().subtract(5, 'months').format('YYYY-MM'), '1'),
          await energy.getAmountYearData(moment().subtract(4, 'months').format('YYYY-MM'), '1'),
          await energy.getAmountYearData(moment().subtract(3, 'months').format('YYYY-MM'), '1'),
          await energy.getAmountYearData(moment().subtract(2, 'months').format('YYYY-MM'), '1'),
          await energy.getAmountYearData(moment().subtract(1, 'months').format('YYYY-MM'), '1'),
        ],
        蒸汽: [
          await energy.getAmountYearData(moment().subtract(6, 'months').format('YYYY-MM'), '2'),
          await energy.getAmountYearData(moment().subtract(5, 'months').format('YYYY-MM'), '2'),
          await energy.getAmountYearData(moment().subtract(4, 'months').format('YYYY-MM'), '2'),
          await energy.getAmountYearData(moment().subtract(3, 'months').format('YYYY-MM'), '2'),
          await energy.getAmountYearData(moment().subtract(2, 'months').format('YYYY-MM'), '2'),
          await energy.getAmountYearData(moment().subtract(1, 'months').format('YYYY-MM'), '2'),
        ],
      },
      xData: [
        moment().subtract(6, 'months').format('MM') + '月',
        moment().subtract(5, 'months').format('MM') + '月',
        moment().subtract(4, 'months').format('MM') + '月',
        moment().subtract(3, 'months').format('MM') + '月',
        moment().subtract(2, 'months').format('MM') + '月',
        moment().subtract(1, 'months').format('MM') + '月',
      ],
    };
    this.success(data);
  }
  async create() {
    const { ctx } = this;
    const { body } = ctx.request;
    const rules = {
      name: { type: 'string', required: true, allowEmpty: false },
      machineData: { type: 'number', required: false, allowEmpty: true },
      position: { type: 'string', required: false, allowEmpty: true },
      createdTime: { type: 'string', required: true, allowEmpty: false },
    };
    try {
      ctx.validate(rules, body);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    body.isAutomatic = false;
    body.entry_time = moment(body.createdTime).format('YYYY-MM');
    body.created_time = body.createdTime;
    // const options = {
    //   where: { name: body.name, entry_time: { [Op.lt]: moment(body.entry_time).format('YYYY-MM') }, isDelete: false },
    //   order: [[ 'entry_time', 'DESC' ]],
    //   limit: 1,
    // };
    // // 上个月的数据
    // const oldUseWater = await ctx.model.YUseWater.findAll(options);
    // if (!_.isEmpty(oldUseWater)) {
    //   if (oldUseWater.machineData !== 0) {
    //     if (body.machineData < oldUseWater[0].machineData) {
    //       return this.badRequest('本月机表数据总量不能小于上个月机表数据总量！');
    //     }
    //   }
    //   body.monthlyDosage = body.machineData - oldUseWater[0].machineData;
    // } else {
    //   body.monthlyDosage = body.machineData;
    // }
    body.monthlyDosage = body.machineData;

    let transaction;
    try {
      transaction = await this.ctx.model.transaction();
      const useWaterOptions = {
        where: { entry_time: moment(body.entry_time).format('YYYY-MM'), name: body.name, isDelete: false },
        order: [[ 'entry_time', 'DESC' ]],
        limit: 1,
      };
      const useWaterData = await ctx.model.YUseWater.findOne(useWaterOptions);
      if (!_.isEmpty(useWaterData)) {
        await useWaterData.destroy();
      }
      // 根据统计日期查询
      const totalEnergyData = await ctx.model.YTotalEnergy.findAll({ where: { created_time: moment(body.entry_time).format('YYYY-MM'), type: '0' } });
      if (!_.isEmpty(totalEnergyData)) {
        totalEnergyData.forEach(async item => {
          await ctx.model.YTotalEnergy.destroy({ where: { id: item.id } }); // 先删除
        });
      }
      // // 查询当前统计日期所有电表的当月用电量
      // const allOptions = {
      //   where: { entry_time: moment(body.entry_time).format('YYYY-MM'), isDelete: false },
      //   attributes: [ 'monthlyDosage' ],
      // };
      // const allUseWater = await ctx.model.YUseWater.findAll(allOptions);
      // let allMonthlyDosage = body.monthlyDosage;
      // allUseWater.forEach(item => {
      //   allMonthlyDosage = Number(allMonthlyDosage) + Number(item.monthlyDosage);
      // });
      const totalEnergyBody = {
        total: !_.isEmpty(totalEnergyData[0]) ? totalEnergyData[0].total : 0,
        amount_total: !_.isEmpty(totalEnergyData[0]) ? totalEnergyData[0].amount_total : 0,
        created_time: moment(body.entry_time).format('YYYY-MM'),
        type: '0',
      };
      await ctx.model.YTotalEnergy.create(totalEnergyBody);
      const data = await ctx.model.YUseWater.create(body);

      await transaction.commit();
      this.success(data);
    } catch (error) {
      await transaction.rollback();
      return this.badRequest(`${error.message}`);
    }
  }
}
module.exports = YUseWaterController;
