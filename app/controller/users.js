'use strict';
const BaseController = require('./base');
// const { SerialPort } = require('serialport');
// const net = require('net');
const _ = require('lodash');
const Op = require('sequelize').Op;
// const socket = new SerialPort({
//   path: 'COM3',
//   baudRate: 9600,
//   Parity: 'none',
//   stopBits: 1,
//   dataBits: 8,
// });

class YUserController extends BaseController {
  // 登录
  async login() {
    const { ctx, app } = this;
    const data = ctx.request.body;
    const rules = {
      nickname: { type: 'string', required: true, allowEmpty: false },
      password: { type: 'string', required: true, allowEmpty: false },
    };
    try {
      ctx.validate(rules, ctx.request.body);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    // 判断该用户是否存在并且密码是否正确
    const user = await ctx.service.user.validUser(data.nickname, data.password);
    if (user) {
      const token = app.jwt.sign({ nickname: data.nickname }, app.config.jwt.secret);
      await this.app.sessionStore.set(token, user, 24 * 3600 * 1000 * 10000);
      this.success({ token, user });
    } else {
      this.badRequest('登录失败');
    }
  }

  // 修改密码
  async editPwd() {
    const { ctx } = this;
    const { body } = ctx.request;
    const rules = {
      oldPassword: { type: 'string', required: true, allowEmpty: false },
      password: { type: 'string', required: true, allowEmpty: false },
    };
    try {
      ctx.validate(rules, body);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    const userStore = ctx.session.app_user;

    const nickname = userStore.nickname;
    // 判断该用户是否存在并且密码是否正确
    const isValidUser = await ctx.service.user.validUser(nickname, body.oldPassword);
    if (isValidUser) {
      const user = await ctx.model.YAdminUser.findByPk(userStore.id);
      const newPwd = ctx.service.user.getMd5Data(body.password);
      await user.update({ password: newPwd });
      this.success('修改成功');
    } else {
      this.badRequest('原密码错误');
    }
  }
  // 重置密码，密码为Adsw1234
  async resetPwd() {
    const { ctx } = this;
    const { params } = ctx;
    const userStore = await ctx.session.app_user;
    if (userStore.role === '2') {
      const user = await ctx.model.YAdminUser.findByPk(params.id);
      const newPwd = ctx.service.user.getMd5Data('Ad@1234');
      await user.update({ password: newPwd });
      this.success('修改成功');
    } else {
      this.unauthorized('您没有此权限请联系管理员');
    }
  }

  /**
   * 用户列表
   */
  async index() {
    const { ctx } = this;
    const { query } = ctx;
    if (_.has(query, 'page_no')) {
      query.page_no = parseInt(query.page_no);
    }
    if (_.has(query, 'page_size')) {
      query.page_size = parseInt(query.page_size);
    }
    const rules = {
      page_no: { type: 'int', required: false, min: 1 },
      page_size: { type: 'int', required: false, min: 1 },
      name: { type: 'string', required: false, allowEmpty: true },
      gender: { type: 'string', required: false, allowEmpty: true },
      phone: { type: 'string', required: false, allowEmpty: true },
      jobNum: { type: 'string', required: false, allowEmpty: true },
      department: { type: 'string', required: false, allowEmpty: true },
      role: { type: 'string', required: false, allowEmpty: true },
      alarmPerson: { type: 'string', required: false, allowEmpty: true },
    };
    try {
      ctx.validate(rules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    const userStore = ctx.session.app_user;

    const page_no = query.page_no || 1;
    const options = {
      where: { [Op.and]: [{ id: { [Op.ne]: userStore.id }, isDelete: false }] },
      attributes: [ 'id', 'nickname', 'name', 'gender', 'phone', 'jobNum', 'department', 'role', 'created_time', 'isApproval', 'isAlarm', 'alarmPerson', 'email', 'operationPermission' ],
      order: [[ 'created_time', 'DESC' ]],
    };
    if (_.has(query, 'nickname') && !_.isEmpty(query.nickname)) {
      options.where.nickname = { [Op.like]: '%' + query.nickname + '%' };
    }
    if (_.has(query, 'name') && !_.isEmpty(query.name)) {
      options.where.name = { [Op.like]: '%' + query.name + '%' };
    }
    if (_.has(query, 'gender') && !_.isEmpty(query.gender)) {
      options.where.gender = query.gender;
    }
    if (_.has(query, 'alarmPerson') && !_.isEmpty(query.alarmPerson)) {
      options.where[Op.and].push(ctx.model.fn('JSON_CONTAINS', ctx.model.col('alarmPerson'), JSON.stringify(query.alarmPerson)));
    }
    if (_.has(query, 'jobNum') && !_.isEmpty(query.jobNum)) {
      options.where.jobNum = { [Op.like]: '%' + query.jobNum + '%' };
    }
    if (_.has(query, 'phone') && !_.isEmpty(query.phone)) {
      options.where.phone = { [Op.like]: '%' + query.phone + '%' };
    }
    if (_.has(query, 'department') && !_.isEmpty(query.department)) {
      options.where.department = { [Op.like]: '%' + query.department + '%' };
    }
    if (_.has(query, 'role') && !_.isEmpty(query.role)) {
      options.where.role = query.role.toString();
    }
    if (_.has(query, 'page_no') && _.has(query, 'page_size')) {
      options.offset = (query.page_no - 1) * query.page_size;
      options.limit = query.page_size;
    }
    if (userStore.role === '2') {
      const data = await ctx.model.YAdminUser.findAndCountAll(options);
      this.service.redis.set('userList', data);
      data.page_no = page_no;
      this.success(data);
    } else {
      this.unauthorized('此页面只能管理员查看');
    }
  }
  /**
   * 一键审批
   */
  async keyApproval() {
    const { ctx } = this;
    const { params } = ctx;
    const paramRules = {
      isOpen: { type: 'string', required: false },
    };
    try {
      ctx.validate(paramRules, params);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    try {
      const userList = await this.service.redis.get('userList');
      if (!_.isEmpty(userList)) {
        await userList.rows.forEach(async item => {
          await ctx.model.YAdminUser.update({ isApproval: params.isOpen }, { where: { id: item.id } });
        });
        this.success('批量修改成功');
      } else {
        return this.badRequest('不符合修改条件');
      }
    } catch (error) {
      return this.badRequest(`${error.message}`);
    }
  }
  /**
   * 审批人列表
   */
  async approverIndex() {
    const { ctx } = this;
    const options = {
      where: { role: '1' },
      attributes: [ 'id', 'name' ],
      order: [[ 'created_time', 'DESC' ]],
    };
    const data = await ctx.model.YAdminUser.findAll(options);
    this.success(data);
  }
  // 创建用户
  async create() {
    const { ctx } = this;
    const { body } = ctx.request;
    const rules = {
      nickname: { type: 'string', required: true, allowEmpty: false },
      password: { type: 'string', required: true, allowEmpty: false },
      name: { type: 'string', required: true, allowEmpty: false },
      gender: { type: 'string', required: true, allowEmpty: false },
      phone: { type: 'string', required: true, allowEmpty: false },
      jobNum: { type: 'string', required: true, allowEmpty: false },
      department: { type: 'string', required: true, allowEmpty: false },
      role: { type: 'string', required: true, allowEmpty: false },
      email: { type: 'string', required: true, allowEmpty: false },
      isAlarm: { type: 'boolean', required: true, allowEmpty: false },
      alarmPerson: { type: 'object', required: false, allowEmpty: true },
      operationPermission: { type: 'object', required: false, allowEmpty: true },
    };
    try {
      ctx.validate(rules, body);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    if (_.has(body, 'nickname')) {
      const options = {
        where: { nickname: body.nickname },
      };
      const count = await ctx.model.YAdminUser.count(options);
      if (count > 0) {
        return this.badRequest('此登录人已存在！');
      }
    }
    const userStore = ctx.session.app_user;

    if (userStore.role === '2') {
      body.isApproval = true;
      body.password = ctx.service.user.getMd5Data(body.password);
      const data = await ctx.model.YAdminUser.create(body);
      this.success(data);
    } else {
      this.unauthorized('此服务只能管理员使用');
    }
  }

  // 修改审批权限
  async updateApproval() {
    const { ctx } = this;
    const { params } = ctx;
    const paramRules = {
      id: { type: 'string', required: true },
    };
    try {
      ctx.validate(paramRules, params);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    const user = await ctx.model.YAdminUser.findByPk(params.id);
    const data = await user.update({ isApproval: !user.isApproval }, { where: params });
    this.success(data);
  }
  // 修改用户
  async update() {
    const { ctx } = this;
    const { params } = ctx;
    const { body } = ctx.request;
    const rules = {
      nickname: { type: 'string', required: false, allowEmpty: false },
      name: { type: 'string', required: false, allowEmpty: false },
      gender: { type: 'string', required: false, allowEmpty: false },
      phone: { type: 'string', required: false, allowEmpty: false },
      jobNum: { type: 'string', required: false, allowEmpty: false },
      department: { type: 'string', required: false, allowEmpty: false },
      role: { type: 'string', required: false, allowEmpty: false },
      email: { type: 'string', required: true, allowEmpty: false },
      isAlarm: { type: 'boolean', required: true, allowEmpty: false },
      alarmPerson: { type: 'object' },
      operationPermission: { type: 'object' },
    };
    try {
      ctx.validate(rules, body);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    if (_.has(body, 'nickname')) {
      const options = {
        where: { id: { [Op.ne]: params.id }, nickname: body.nickname },
      };
      const count = await ctx.model.YAdminUser.count(options);
      if (count > 0) {
        return this.badRequest('此登录人已存在！');
      }
    }
    delete body.password;
    if (!body.isAlarm) {
      delete body.alarmPerson;
    }
    await ctx.model.YAdminUser.update(body, { where: params });
    const data = await ctx.model.YAdminUser.findOne({ where: params });

    this.success(data);
  }

  // 删除用户
  async destroy() {
    const { ctx } = this;
    const { params } = ctx;

    const paramRules = {
      id: { type: 'string', required: true },
    };
    try {
      ctx.validate(paramRules, params);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    const userStore = ctx.session.app_user;

    if (userStore.role === '2') {
      await ctx.model.YAdminUser.update({ isDelete: true }, { where: { id: params.id } });

      this.success('删除成功');
    } else {
      this.unauthorized('您没有此权限请联系管理员');
    }
  }
  /**
     * 用户列表
     */
  async testIndex() {
    const SerialPort = require('serialport');
    const port = SerialPort('COM3', {
      baudRate: 115200,
      stopBits: 1,
      databits: 8, // 数据位
      parity: 'none', // 校验位
    });
    console.log('port', port);
    // this.success(data);
  }
}
module.exports = YUserController;
