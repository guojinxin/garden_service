/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-23 09:40:58
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-09-07 08:58:45
 * @FilePath: \garden_service\app\controller\upload.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const fs = require('fs');
const path = require('path');
const moment = require('moment');
const mkdirp = require('mkdirp');
const BaseController = require('./base');


class UploadController extends BaseController {
  async uploadAvatar() {
    const { ctx, config } = this;
    try {
      // 0、获取文件
      const file = ctx.request.files[0];
      // ctx.request.files[0] 表示获取第一个文件，若前端上传多个文件则可以遍历这个数组对象
      const fileData = fs.readFileSync(file.filepath);
      // 1、获取当前日期
      const day = moment(new Date()).format('YYYYMMDD');
      // 2、创建图片保存的路径
      const dir = path.join(config.uploadAvatarDir, day);
      // 3、创建目录
      await mkdirp(dir);
      // 4、生成路径返回
      const date = Date.now(); // 毫秒数
      const tempDir = path.join(dir, date + path.extname(file.filename)); // 返回图片保存的路径
      // 5、写入文件夹
      fs.writeFileSync(tempDir, fileData);
      this.success(this.ctx.origin + tempDir.slice(3).replace(/\\/g, '/'));
    } catch (error) {
      this.badRequest('上传失败');
    } finally {
      // 6、清除临时文件
      ctx.cleanupRequestFiles();
    }
  }
}

module.exports = UploadController;
