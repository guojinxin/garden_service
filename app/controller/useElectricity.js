
'use strict';
const BaseController = require('./base');
const Op = require('sequelize').Op;
const _ = require('lodash');
const moment = require('moment');
class YUseElectricityController extends BaseController {
  async index() {
    const { ctx } = this;
    const { query } = ctx;
    if (_.has(query, 'page_no')) {
      query.page_no = parseInt(query.page_no);
    }
    if (_.has(query, 'page_size')) {
      query.page_size = parseInt(query.page_size);
    }
    const rules = {
      page_no: { type: 'int', required: false, min: 1 },
      page_size: { type: 'int', required: false, min: 1 },
      name: { type: 'string', required: false, allowEmpty: true },
      entryTimeStr: { type: 'string', required: false, allowEmpty: true },
      entryTimeEnd: { type: 'string', required: false, allowEmpty: true },
    };
    try {
      ctx.validate(rules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    const page_no = query.page_no || 1;
    const options = {
      where: { isDelete: false },
      order: [[ 'created_time', 'DESC' ]],
    };
    if (_.has(query, 'name') && !_.isEmpty(query.name)) {
      options.where.name = { [Op.like]: '%' + query.name + '%' };
    }
    if (_.has(query, 'entryTimeStr') && !_.isEmpty(query.entryTimeStr)) {
      options.where.entry_time = { [Op.gte]: moment(query.entryTimeStr).format('YYYY-MM'), [Op.lte]: moment(query.entryTimeEnd).format('YYYY-MM') };
    }
    if (_.has(query, 'page_no') && _.has(query, 'page_size')) {
      options.offset = (query.page_no - 1) * query.page_size;
      options.limit = query.page_size;
    }
    const data = await ctx.model.YUseElectricity.findAndCountAll(options);
    data.page_no = page_no;
    this.success(data);
  }
  async electricityStatistics() {
    const { ctx } = this;
    const { energy } = ctx.service;
    const data = {
      去年: [
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-01', '1'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-02', '1'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-03', '1'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-04', '1'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-05', '1'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-06', '1'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-07', '1'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-08', '1'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-09', '1'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-10', '1'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-11', '1'),
        await energy.getWaterLastYearData(moment().add(-1, 'year').format('YYYY') + '-12', '1'),
      ],
      今年: [
        await energy.getWaterLastYearData(moment().format('YYYY') + '-01', '1'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-02', '1'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-03', '1'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-04', '1'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-05', '1'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-06', '1'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-07', '1'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-08', '1'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-09', '1'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-10', '1'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-11', '1'),
        await energy.getWaterLastYearData(moment().format('YYYY') + '-12', '1'),
      ],
    };
    this.success(data);
  }
  async create() {
    const { ctx } = this;
    const { body } = ctx.request;
    const rules = {
      name: { type: 'string', required: true, allowEmpty: false },
      machineData: { type: 'number', required: false, allowEmpty: true },
      position: { type: 'string', required: false, allowEmpty: true },
      createdTime: { type: 'string', required: true, allowEmpty: false },
    };
    try {
      ctx.validate(rules, body);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    body.isAutomatic = false;
    body.entry_time = moment(body.createdTime).format('YYYY-MM');
    body.created_time = body.createdTime;
    // const options = {
    //   where: { name: body.name, entry_time: { [Op.lt]: moment(body.entry_time).format('YYYY-MM') }, isDelete: false },
    //   order: [[ 'entry_time', 'DESC' ]],
    //   limit: 1,
    // };
    // // 上个月的数据
    // const oldUseWater = await ctx.model.YUseElectricity.findAll(options);
    // if (!_.isEmpty(oldUseWater)) {
    //   if (oldUseWater.machineData !== 0) {
    //     if (body.machineData < oldUseWater[0].machineData) {
    //       return this.badRequest('本月机表数据总量不能小于上个月机表数据总量！');
    //     }
    //   }
    //   body.monthlyDosage = body.machineData - oldUseWater[0].machineData;
    // } else {
    //   body.monthlyDosage = body.machineData;
    // }
    body.monthlyDosage = body.machineData;

    let transaction;
    try {
      transaction = await this.ctx.model.transaction();
      const useWaterOptions = {
        where: { entry_time: moment(body.entry_time).format('YYYY-MM'), name: body.name, isDelete: false },
        order: [[ 'entry_time', 'DESC' ]],
        limit: 1,
      };
      const useWaterData = await ctx.model.YUseElectricity.findOne(useWaterOptions);
      if (!_.isEmpty(useWaterData)) {
        await useWaterData.destroy();
      }
      // 根据统计日期查询
      const totalEnergyData = await ctx.model.YTotalEnergy.findAll({ where: { created_time: moment(body.entry_time).format('YYYY-MM'), type: '1' } });
      if (!_.isEmpty(totalEnergyData)) {
        totalEnergyData.forEach(async item => {
          await ctx.model.YTotalEnergy.destroy({ where: { id: item.id } }); // 先删除
        });
      }
      // // 查询当前统计日期所有电表的当月用电量
      // const allOptions = {
      //   where: { entry_time: moment(body.entry_time).format('YYYY-MM'), isDelete: false },
      //   attributes: [ 'monthlyDosage' ],
      // };
      // const allUseWater = await ctx.model.YUseElectricity.findAll(allOptions);
      // let allMonthlyDosage = body.monthlyDosage;
      // allUseWater.forEach(item => {
      //   allMonthlyDosage = Number(allMonthlyDosage) + Number(item.monthlyDosage);
      // });
      const totalEnergyBody = {
        total: !_.isEmpty(totalEnergyData[0]) ? totalEnergyData[0].total : 0,
        amount_total: !_.isEmpty(totalEnergyData[0]) ? totalEnergyData[0].amount_total : 0,
        created_time: moment(body.entry_time).format('YYYY-MM'),
        type: '1',
      };
      await ctx.model.YTotalEnergy.create(totalEnergyBody);
      const data = await ctx.model.YUseElectricity.create(body);

      await transaction.commit();
      this.success(data);
    } catch (error) {
      await transaction.rollback();
      return this.badRequest(`${error.message}`);
    }
  }
}
module.exports = YUseElectricityController;
