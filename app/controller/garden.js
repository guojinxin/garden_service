/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-09-21 15:43:07
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-05-06 16:05:30
 * @FilePath: \garden_service\app\controller\garden.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const BaseController = require('./base');
const cheerio = require('cheerio');
const superagent = require('superagent');
class YGarden extends BaseController {
  async index() {
    const environmentData = await this.ctx.environmentModel.Environment.findAll({});
    const windDirection = [{
      name: '北风',
      value: '0.0',
    }, {
      name: '东北风',
      value: '0.1',
    }, {
      name: '东风',
      value: '0.2',
    }, {
      name: '东南风',
      value: '0.3',
    }, {
      name: '南风',
      value: '0.4',
    }, {
      name: '西南风',
      value: '0.5',
    }, {
      name: '西北风',
      value: '0.7',
    }];
    const data = [];
    environmentData.forEach(element => {

      if (element.leixing === '风力') {
        data.push({
          name: element.leixing,
          value: element.shuzhi * 10,
          unit: '级',
        });
      }
      if (element.leixing === '风速') {
        data.push({
          name: element.leixing,
          value: element.shuzhi,
          unit: '米/秒',
        });
      }
      if (element.leixing === '风向') {
        data.push({
          name: element.leixing,
          value: windDirection.find(item => item.value === element.shuzhi).name,
          unit: '',
        });
      }
      if (element.leixing === '温度') {
        data.push({
          name: element.leixing,
          value: element.shuzhi,
          unit: '°C',
        });
      }
      if (element.leixing === '湿度') {
        data.push({
          name: element.leixing,
          value: element.shuzhi,
          unit: '%rh',
        });
      }
      if (element.leixing === '噪音') {
        data.push({
          name: element.leixing,
          value: element.shuzhi,
          unit: '分贝',
        });
      }
      if (element.leixing === 'PM10') {
        data.push({
          name: element.leixing,
          value: element.shuzhi * 10,
          unit: 'μg/m3',
        });
      }
      if (element.leixing === 'PM2.5') {
        data.push({
          name: element.leixing,
          value: element.shuzhi * 10,
          unit: 'μg/m3',
        });
      }
    });
    return this.success(data);
  }
  async getWeather() {
    /**
     * index.js
     * [description] - 使用superagent.get()方法来访问百度新闻首页
     */
    const res = await superagent.get('https://tianqi.2345.com/donge1d/70613.htm');
    const weatherList = [];
    // 访问成功，请求https://tianqi.2345.com/donge1d/70613.htm'页面所返回的数据会包含在res
    const $ = cheerio.load(res.text);
    // 温度
    const temperature = $('.real-t').text();
    // 天气图标
    const cludy = $('.cludy').text();
    // 空气质量
    const realRankText = $('.real-icon .real-rank').text();
    if (realRankText === '优') {
      weatherList.push({ weatherColor: 'background:green' });
    } else if (realRankText === '良') {
      weatherList.push({ weatherColor: 'background:yellow' });
    } else if (realRankText === '轻度污染') {
      weatherList.push({ weatherColor: 'background:orange' });
    } else if (realRankText === '中度污染') {
      weatherList.push({ weatherColor: 'background:red' });
    } else if (realRankText === '重度污染') {
      weatherList.push({ weatherColor: 'background:red' });
    } else {
      weatherList.push({ weatherColor: 'background:maroon' });
    }
    weatherList[0].weather = temperature.replace('°', '℃');
    weatherList[0].sunny = $('.real-today span').text().toString()
      .replace(/\s*/g, '')
      .split('°')[1];
    weatherList[0].temperatureRange = $('.real-today span').text().slice($('.real-today span').text().indexOf('：') + 1, $('.real-today span').text().indexOf('°')) + '℃';
    weatherList[0].temperatureState = realRankText;
    weatherList[0].cludy = cludy;

    // 南风 2级湿度 50%紫外线 强气压 1011hPa
    await $('.real-data span').each((idx, ele) => {
      if (idx === 0) {
        weatherList[0].windDirection = $(ele).text().replace(/^\s+|\s+$/g, '')
          .split(' ')[0];
        weatherList[0].windSpeed = $(ele).text().replace(/^\s+|\s+$/g, '')
          .split(' ')[1];
      } else if (idx === 1) {
        weatherList[0].humidity = $(ele).text().replace(/^\s+|\s+$/g, '')
          .split(' ')[1];
      }
    });
    // PM2.5 PM10 O3 NO2 SO2 CO
    await $('.aqi-map-style-tip em').each((idx, ele) => {
      if (idx === 0) {
        weatherList[0].pmTwoPointFive = $(ele).text().replace(/^\s+|\s+$/g, '');
      }
      if (idx === 1) {
        weatherList[0].pmTen = $(ele).text().replace(/^\s+|\s+$/g, '');
      }
    });
    return this.success(weatherList);
  }
}
module.exports = YGarden;
