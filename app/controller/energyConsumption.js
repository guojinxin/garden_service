/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2023-02-02 14:10:10
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-04-26 12:02:56
 * @FilePath: \garden_service\app\controller\energyConsumption.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const BaseController = require('./base');
const _ = require('lodash');
const moment = require('moment');
const Op = require('sequelize').Op;
class EnergyConsumptionController extends BaseController {
  async index() {
    const { ctx } = this;
    const { query } = ctx;
    if (_.has(query, 'page_no')) {
      query.page_no = parseInt(query.page_no);
    }
    if (_.has(query, 'page_size')) {
      query.page_size = parseInt(query.page_size);
    }
    const rules = {
      page_no: { type: 'int', required: false, min: 1 },
      page_size: { type: 'int', required: false, min: 1 },
      type: { type: 'string', required: false, allowEmpty: true },
      entryTimeStr: { type: 'string', required: false, allowEmpty: true },
      entryTimeEnd: { type: 'string', required: false, allowEmpty: true },

    };
    try {
      ctx.validate(rules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    const page_no = query.page_no || 1;
    const options = {
      where: {},
      order: [[ 'created_time', 'DESC' ]],
    };
    if (_.has(query, 'type') && !_.isEmpty(query.type)) {
      options.where.type = query.type;
    }
    if (_.has(query, 'entryTimeStr') && !_.isEmpty(query.entryTimeStr)) {
      options.where.created_time = { [Op.gte]: moment(query.entryTimeStr).format('YYYY-MM'), [Op.lte]: moment(query.entryTimeEnd).format('YYYY-MM') };
    }
    if (_.has(query, 'page_no') && _.has(query, 'page_size')) {
      options.offset = (query.page_no - 1) * query.page_size;
      options.limit = query.page_size;
    }
    const data = await ctx.model.YTotalEnergy.findAndCountAll(options);
    data.page_no = page_no;
    this.success(data);
  }
  async update() {
    const { ctx } = this;
    const { params } = ctx;
    const { body } = ctx.request;
    await ctx.model.YTotalEnergy.update({ amount_total: body.amountTotal, total: body.total }, { where: params });
    const data = await ctx.model.YTotalEnergy.findOne({ where: params });
    this.success(data);

  }
}
module.exports = EnergyConsumptionController;
