/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-28 20:57:08
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-09-08 11:49:14
 * @FilePath: \garden_service\app\controller\son_visit.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const BaseController = require('./base');
const Op = require('sequelize').Op;
const _ = require('lodash');
const moment = require('moment');

class YParkController extends BaseController {
  /**
   * 门岗访客
   */
  async index() {
    const { ctx } = this;
    const options = {
      where: { statusName: { [Op.in]: [ '已到访', '已离开' ] }, created_time: {
        [Op.lt]: moment().format('YYYY-MM-DD') + ' 23:59:59',
        [Op.gt]: moment().format('YYYY-MM-DD') + ' 00:00:00',
      } },
      attributes: [ 'visit_Id' ],
      order: [[ 'created_time', 'DESC' ]],
    };
    const visitStateData = await ctx.model.YVisitState.findAll(options);
    const visitIdList = _.map(visitStateData, 'visit_Id');
    const visitOptions = {
      where: { visitId: { [Op.in]: visitIdList } },
      attributes: [ 'visitId', 'visitUnit', 'name', 'licensePlateNum', 'updated_time', 'address', 'status' ],
      order: [[ 'updated_time', 'DESC' ]],
    };
    const visitUserData = await ctx.model.YVisitUser.findAll(visitOptions);
    const visitData = {};
    visitData.titleName = [ '来访单位', '姓名', '车牌号', '拜访到达/离开时间', '接访地点', '状态' ];
    const valueAry = [];

    const arr1 = _.uniqBy(visitUserData, 'visitId');
    arr1.forEach(element => {
      delete element.dataValues.visitId;
      element.dataValues.updated_time = moment(element.dataValues.updated_time).format('YYYY-MM-DD HH:mm:ss');
      element.dataValues.status = element.dataValues.status === '4' ? '已到访' : '已离开';
      valueAry.push(Object.values(element.dataValues));
    });
    visitData.rowData = valueAry;
    this.success(visitData);
  }
  /**
   * 道闸部门
   */
  async sluiceDepartment() {
    const { ctx } = this;
    const option = {
      where: { card_zt: '启用' },
      group: [ 'card_bz' ],
      attributes: [
        [
          this.ctx.attendanceMssql.Sequelize.fn('COUNT', 'card_bz'),
          'card_count',
        ],
        'card_bz',
      ],
      raw: true,
    };
    const data = await ctx.mssql.Mfeecard.findAll(option);
    this.success(data);
  }
  async sluiceList() {
    const { ctx } = this;
    const { query } = ctx;
    if (_.has(query, 'page_no')) {
      query.page_no = parseInt(query.page_no);
    }
    if (_.has(query, 'page_size')) {
      query.page_size = parseInt(query.page_size);
    }
    const rules = {
      page_no: { type: 'int', required: false, min: 1 },
      page_size: { type: 'int', required: false, min: 1 },
    };
    try {
      ctx.validate(rules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(
        `${error.message}: ${error.errors[0].code} ${error.errors[0].field}`
      );
    }
    const mFeeOptions = {
      where: {},
      model: ctx.mssql.Mfeecard,
      as: 'mFeeCard',
      attributes: [ 'card_bz' ],
      raw: true,
      distinct: true,
    };
    // 根据部门
    if (_.has(query, 'card_bz') && !_.isEmpty(query.card_bz)) {
      mFeeOptions.where.card_bz = { [Op.like]: '%' + query.card_bz + '%' };
    }
    const page_no = query.page_no || 1;
    const options = {
      where: {},
      include: [ mFeeOptions ],
      attributes: [ 'car_cp', 'user_name', 'in_time', 'out_time' ],
      order: [[ 'in_time', 'DESC' ]],
    };
    if (_.has(query, 'user_name') && !_.isEmpty(query.user_name)) {
      options.where.user_name = { [Op.like]: '%' + query.user_name + '%' };
    }
    if (_.has(query, 'inTimeStr') && !_.isEmpty(query.inTimeStr)) {
      options.where.in_time = {
        [Op.gte]: query.inTimeStr,
        [Op.lte]: query.inTimeEnd,
      };
    }
    if (_.has(query, 'outTimeStr') && !_.isEmpty(query.outTimeStr)) {
      options.where.out_time = {
        [Op.gte]: query.outTimeStr,
        [Op.lte]: query.outTimeEnd,
      };
    }
    if (_.has(query, 'page_no') && _.has(query, 'page_size')) {
      options.offset = (query.page_no - 1) * query.page_size;
      options.limit = query.page_size;
    }
    const data = await ctx.mssql.Parked.findAndCountAll(options);
    data.page_no = page_no;
    this.success(data);
  }
}

module.exports = YParkController;
