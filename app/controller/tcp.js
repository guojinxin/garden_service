/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-09-18 18:18:48
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-09-22 10:31:28
 * @FilePath: \garden_service\app\controller\tcp.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// /*
//  * @Author: guojinxin 1907745233@qq.com
//  * @Date: 2022-09-18 18:18:48
//  * @LastEditors: guojinxin 1907745233@qq.com
//  * @LastEditTime: 2022-09-19 09:31:16
//  * @FilePath: \garden_service\app\controller\tcp.js
//  * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
//  */

// // tcp.js

// const net = require('net');

// class tcp {
//   constructor(args) {
//     this.client = new net.Socket();
//     this.host = args.host || '127.0.0.1';
//     this.port = args.port || '502l';
//     this.init();
//   }
//   init() {
//     this.client.on('connect', () => {
//       console.log('connect');
//     });
//     this.client.on('error', err => {
//       console.log('error', err);
//     });
//     this.client.on('close', () => {
//       console.log('close,reconnect again!');
//       setTimeout(() => {
//         this.client.end();
//         this.connectServer();
//       }, 10000);
//     });
//     this.connectServer();
//   }
//   connectServer() {
//     this.client.connect({
//       host: this.host,
//       port: this.port,
//     });
//   }
//   readHoldRegs(arr) {
//     return new Promise(resolve => {
//       this.client.write(Buffer.from(arr));
//       this.client.once('data', data => {
//         resolve(data);
//       });
//     });
//   }
// }

// module.exports = tcp;
