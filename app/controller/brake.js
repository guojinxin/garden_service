/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-11-09 14:02:13
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-03-16 09:40:30
 * @FilePath: \garden_service\app\controller\brake.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

'use strict';
const BaseController = require('./base');
const sequelize = require('sequelize');
const moment = require('moment');
// sequelize.DATE.prototype._stringify = function _stringify(date, options) {
//   return this._applyTimezone(date, options).format('YYYY-MM-DD HH:mm:ss.SSS');
// };
class ParkedController extends BaseController {
  async index() {
    const { ctx } = this;
    const options = {
      where: {
        in_time: {
          [sequelize.Op.lt]: moment().format('YYYY-MM-DD') + ' 23:59:59',
          [sequelize.Op.gt]: moment().format('YYYY-MM-DD') + ' 00:00:00',
        },
      },
      attributes: [ 'car_cp', 'user_name', 'in_time', 'out_time' ],
      order: [[ 'in_time', 'DESC' ]],
    };
    const parkedData = {};
    const data = await ctx.mssql.Parked.findAll(options);
    parkedData.titleName = [ '车牌号', '人员姓名', '进入时间', '出去时间' ];
    const valueAry = [];
    data.forEach(element => {
      element.dataValues.in_time = moment(element.dataValues.in_time)
        // .subtract(8, 'hours')
        .format('YYYY-MM-DD HH:mm:ss');
      if (element.dataValues.out_time !== null) {
        element.dataValues.out_time = moment(element.dataValues.out_time)
          // .subtract(8, 'hours')
          .format('YYYY-MM-DD HH:mm:ss');
      } else {
        element.dataValues.out_time = '--';
      }
      valueAry.push(Object.values(element.dataValues));
    });
    parkedData.rowData = valueAry;
    this.success(parkedData);
  }
  async parkedStatistics() {
    const { ctx } = this;
    const parkedAll = await ctx.mssql.Parked.findAndCountAll({
      where: {
        in_time: {
          [sequelize.Op.lt]: moment().format('YYYY-MM-DD') + ' 23:59:59',
          [sequelize.Op.gt]: moment().format('YYYY-MM-DD') + ' 00:00:00',
        },
      },
      attributes: [ 'parked_id' ],
    });
    const outParkedAll = await ctx.mssql.Parked.findAndCountAll({
      where: { in_time: {
        [sequelize.Op.lt]: moment().format('YYYY-MM-DD') + ' 23:59:59',
        [sequelize.Op.gt]: moment().format('YYYY-MM-DD') + ' 00:00:00',
      }, out_time: { [sequelize.Op.ne]: null } },
      attributes: [ 'parked_id' ],
    });
    let usedParked = parkedAll.count - outParkedAll.count;
    if (usedParked > 46) {
      usedParked = 46;
    } else if (usedParked <= 0) {
      usedParked = '0';
    }
    const data = {
      key1: 46,
      key2: usedParked,
      key3: 46 - usedParked === 0 ? '0' : 46 - usedParked,
      key4: [
        {
          accountedFor: [
            Number((usedParked / 46).toFixed(2)),
            Number((usedParked / 46 + 0.05).toFixed(2)),
          ],
        },
      ],
    };
    this.success(data);
  }
  async parkedStatisticsLine() {
    const { ctx } = this;
    const data = {
      xData: [
        moment().subtract(4, 'days').format('DD') + '日',
        moment().subtract(3, 'days').format('DD') + '日',
        moment().subtract(2, 'days').format('DD') + '日',
        moment().subtract(1, 'days').format('DD') + '日',
        moment().format('DD') + '日',
      ],
      chartdata: {
        南门: [
          await ctx.service.parked.getParkedStatistics(
            moment().subtract(4, 'days').format('YYYY-MM-DD'),
            moment().subtract(3, 'days').format('YYYY-MM-DD')
          ),
          await ctx.service.parked.getParkedStatistics(
            moment().subtract(3, 'days').format('YYYY-MM-DD'),
            moment().subtract(2, 'days').format('YYYY-MM-DD')
          ),
          await ctx.service.parked.getParkedStatistics(
            moment().subtract(2, 'days').format('YYYY-MM-DD'),
            moment().subtract(1, 'days').format('YYYY-MM-DD')
          ),
          await ctx.service.parked.getParkedStatistics(
            moment().subtract(1, 'days').format('YYYY-MM-DD'),
            moment().format('YYYY-MM-DD')
          ),
          await ctx.service.parked.getParkedStatistics(
            moment().format('YYYY-MM-DD')
          ),
        ],
        北门: [ 0, 0, 0, 0, 0 ],
      },
    };
    this.success(data);
  }
  async accessControl() {
    const { ctx } = this;
    const options = {
      where: {
        in_time: {
          [sequelize.Op.lt]: moment().format('YYYY-MM-DD') + ' 23:59:59',
          [sequelize.Op.gt]: moment().format('YYYY-MM-DD') + ' 00:00:00',
        },
      },
      attributes: [ 'car_cp', 'in_time', 'out_time' ],
      order: [[ 'in_time', 'DESC' ]],
    };
    const parkedData = {};
    const data = await ctx.mssql.Parked.findAll(options);
    parkedData.titleName = [ '车牌号', '进入时间', '出去时间' ];
    const valueAry = [];
    data.forEach(element => {
      element.dataValues.in_time = moment(element.dataValues.in_time)
        // .subtract(8, 'hours')
        .format('YYYY-MM-DD HH:mm:ss');
      if (element.dataValues.out_time !== null) {
        element.dataValues.out_time = moment(element.dataValues.out_time)
          // .subtract(8, 'hours')
          .format('YYYY-MM-DD HH:mm:ss');
      } else {
        element.dataValues.out_time = '--';
      }
      valueAry.push(Object.values(element.dataValues));
    });
    parkedData.rowData = valueAry;
    this.success(parkedData);
  }
}

module.exports = ParkedController;
