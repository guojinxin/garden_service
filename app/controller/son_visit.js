/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-28 20:57:08
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-07-11 13:27:17
 * @FilePath: \garden_service\app\controller\son_visit.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const BaseController = require('./base');
// const Op = require('sequelize').Op;


class YUserController extends BaseController {
  /**
   * 新增批量访客
   */
  async create() {
    const { ctx } = this;
    const { body } = ctx.request;
    const rules = {
      name: { type: 'string', required: true, allowEmpty: false },
      idNum: { type: 'string', required: false, allowEmpty: true },
      licensePlateNum: { type: 'string', required: false, allowEmpty: true },
      healthCode: { type: 'string', required: false, allowEmpty: true },
      tourCode: { type: 'string', required: false, allowEmpty: true },
      visit_id: { type: 'string', required: true, allowEmpty: false },
    };
    try {
      ctx.validate(rules, body);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    try {
      const data = await ctx.model.YSonVisitUser.create(body);
      this.success(data);
    } catch (error) {
      return this.badRequest(`${error.message}`);
    }
  }
  // 修改批量访客
  async update() {
    const { ctx } = this;
    const { params } = ctx;
    const { body } = ctx.request;
    const rules = {
      name: { type: 'string', required: true, allowEmpty: false },
      idNum: { type: 'string', required: false, allowEmpty: true },
      licensePlateNum: { type: 'string', required: false, allowEmpty: true },
      healthCode: { type: 'string', required: false, allowEmpty: true },
      tourCode: { type: 'string', required: false, allowEmpty: true },
    };
    try {
      ctx.validate(rules, body);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    const data = await ctx.model.YSonVisitUser.update(body, { where: params });
    this.success(data);
  }
  // 删除用户
  async destroy() {
    const { ctx } = this;
    const { params } = ctx;
    try {
      const data = await ctx.model.YSonVisitUser.destroy({ where: params });
      this.success(data);
    } catch (error) {
      return this.badRequest(`${error.message}`);
    }
  }
  /**
   *  子访客列表
   */
  async visit_index() {
    const { ctx } = this;
    const { query } = ctx;
    const rules = {
      visitId: { type: 'string', required: false },
    };
    try {
      ctx.validate(rules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    const options = {
      where: { visit_id: query.visitId },
      order: [[ 'created_time', 'DESC' ]],
    };
    const data = await ctx.model.YSonVisitUser.findAndCountAll(options);
    this.success(data);
  }
  /**
   * 访客列表（扫描二维码进入）
   */
  async index() {
    const { ctx } = this;
    const { query } = ctx;
    const rules = {
      visitId: { type: 'string', required: false },
    };
    try {
      ctx.validate(rules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    const options = {
      where: { id: query.visitId },
      include: [{ model: ctx.model.YAdminUser, as: 'adminUser', attributes: [ 'id', 'name', 'gender', 'phone', 'jobNum', 'department', 'isApproval', 'role' ], required: false },
        { model: ctx.model.YAdminUser, as: 'approverUser', attributes: [ 'id', 'name', 'gender', 'phone', 'jobNum', 'department', 'isApproval', 'role' ], required: false },
        { model: ctx.model.YSonVisitUser, as: 'children', attributes: [ 'id', 'name', 'idNum', 'licensePlateNum', 'healthCode', 'tourCode', 'visit_id' ] }],
      attributes: [ 'id', 'visitId', 'name', 'idNum', 'address', 'visitStrTime', 'visitEndTime', 'status', 'visitUnit', 'licensePlateNum', 'healthCode', 'tourCode', 'describe', 'createUser_Id', 'approver_Id', 'estimatedStrTime', 'estimatedEndTime', 'rejectDescribe', 'actualVisit', 'remark', 'created_time', 'isContinuousVisit', 'isOutboundFreight' ],
      order: [[ 'created_time', 'DESC' ]],
    };
    const userStore = ctx.session.app_user;

    if (userStore.role === '2' || userStore.role === '3') {
      const data = await ctx.model.YVisitUser.findAndCountAll(options);
      this.success(data);
    } else {
      this.unauthorized('您没有此权限');
    }
  }
}

module.exports = YUserController;
