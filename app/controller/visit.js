'use strict';
const BaseController = require('./base');
const _ = require('lodash');
const Op = require('sequelize').Op;
const moment = require('moment');
const uuid = require('uuid');
const nodemailer = require('nodemailer');

class YVisitController extends BaseController {
  // 创建访客
  async create() {
    const { ctx } = this;
    const { body } = ctx.request;
    const rules = {
      name: { type: 'string', required: true, allowEmpty: false },
      idNum: { type: 'string', required: false, allowEmpty: true },
      visitStrTime: { type: 'string', required: false, allowEmpty: true },
      visitEndTime: { type: 'string', required: false, allowEmpty: true },
      healthCode: { type: 'string', required: false, allowEmpty: true },
      tourCode: { type: 'string', required: false, allowEmpty: true },
      address: { type: 'string', required: true, allowEmpty: false },
      describe: { type: 'string', required: false, allowEmpty: true },
      visitUnit: { type: 'string', required: true, allowEmpty: false },
      licensePlateNum: { type: 'string', required: false, allowEmpty: true },
      approver_Id: { type: 'string', required: false, allowEmpty: true },
      estimatedStrTime: { type: 'string', required: false },
      estimatedEndTime: { type: 'string', required: false },
      phone: { type: 'string', required: false },
      isContinuousVisit: { type: 'boolean', required: false, allowEmpty: true },
      isOutboundFreight: { type: 'boolean', required: false, allowEmpty: true },
      visitList: { type: 'object' },
    };
    try {
      ctx.validate(rules, body);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(
        `${error.message}: ${error.errors[0].code} ${error.errors[0].field}`
      );
    }
    const userStore = ctx.session.app_user;
    if (!body.isContinuousVisit) {
      body.visitStrTime =
        moment(body.visitEndTime).format('YYYY-MM-DD') + ' 00:00:00';
    }
    if (userStore.isApproval) {
      if (body.approver_Id === userStore.id || userStore.role === '2') {
        body.status = '1';
      } else {
        body.status = '0';
      }
    } else {
      body.status = '1';
    }
    body.id = uuid.v1();
    const t = await ctx.model.transaction();
    body.createUser_Id = userStore.id;
    try {
      const data = await ctx.model.YVisitUser.create(body, { transaction: t });
      const visitBody = {
        statusName: body.status === '1' ? '已审批' : '待审批',
        createUser_Id: userStore.id,
        visit_Id: data.visitId,
        describe: '添加访客',
      };
      await ctx.model.YVisitState.create(visitBody, { transaction: t });
      if (!_.isEmpty(body.visitList)) {
        const visitData = [];
        await body.visitList.forEach(item => {
          item.visit_id = data.visitId;
          visitData.push(item);
        });
        await ctx.model.YSonVisitUser.bulkCreate(body.visitList, {
          transaction: t,
        });
      }
      await t.commit();
      this.success(data);
    } catch (error) {
      await t.rollback();
      return this.badRequest(`${error.message}`);
    }
  }
  async getOutboundFreightImage() {
    const { ctx } = this;
    const { query } = ctx;
    const paramRules = {
      id: { type: 'string', required: true },
    };
    try {
      ctx.validate(paramRules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(
        `${error.message}: ${error.errors[0].code} ${error.errors[0].field}`
      );
    }
    try {
      const visitUserData = await ctx.model.YVisitUser.findOne({
        where: query,
        attributes: [ 'outboundFreightImage' ],
      });
      this.success(visitUserData);
    } catch (error) {
      return this.badRequest(`${error.message}`);
    }
  }
  // 修改货物照片
  async updateOutboundFreightImage() {
    const { ctx } = this;
    const { params } = ctx;
    const { body } = ctx.request;
    const rules = {
      outboundFreightImage: {
        type: 'array',
        required: true,
        allowEmpty: false,
      },
    };
    const paramRules = {
      id: { type: 'string', required: true },
    };
    try {
      ctx.validate(paramRules, params);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(
        `${error.message}: ${error.errors[0].code} ${error.errors[0].field}`
      );
    }
    try {
      ctx.validate(rules, body);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(
        `${error.message}: ${error.errors[0].code} ${error.errors[0].field}`
      );
    }
    try {
      const visitUserData = await ctx.model.YVisitUser.findOne({
        where: params,
      });
      const data = await visitUserData.update(body, {
        where: { ...params },
      });
      this.success(data);
    } catch (error) {
      return this.badRequest(`${error.message}`);
    }
  }
  // 修改访客信息
  async update() {
    const { ctx } = this;
    const { params } = ctx;
    const { body } = ctx.request;
    const rules = {
      name: { type: 'string', required: true, allowEmpty: false },
      idNum: { type: 'string', required: false, allowEmpty: true },
      visitStrTime: { type: 'string', required: false, allowEmpty: true },
      visitEndTime: { type: 'string', required: false, allowEmpty: true },
      healthCode: { type: 'string', required: false, allowEmpty: true },
      tourCode: { type: 'string', required: false, allowEmpty: true },
      address: { type: 'string', required: true, allowEmpty: false },
      describe: { type: 'string', required: false, allowEmpty: true },
      visitUnit: { type: 'string', required: true, allowEmpty: false },
      licensePlateNum: { type: 'string', required: false, allowEmpty: true },
      approver_Id: { type: 'string', required: false, allowEmpty: true },
      estimatedStrTime: { type: 'string', required: false },
      estimatedEndTime: { type: 'string', required: false },
      isContinuousVisit: { type: 'boolean', required: true, allowEmpty: false },
      isOutboundFreight: { type: 'boolean', required: true, allowEmpty: false },
      phone: { type: 'string', required: false },
      visitList: { type: 'object' },
    };
    const paramRules = {
      visitId: { type: 'string', required: true },
    };
    try {
      ctx.validate(paramRules, params);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(
        `${error.message}: ${error.errors[0].code} ${error.errors[0].field}`
      );
    }
    try {
      ctx.validate(rules, body);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(
        `${error.message}: ${error.errors[0].code} ${error.errors[0].field}`
      );
    }
    const t = await ctx.model.transaction();
    const userStore = ctx.session.app_user;
    if (userStore.isApproval) {
      if (body.approver_Id === userStore.id || userStore.role === '2') {
        body.status = '1';
      } else {
        body.status = '0';
      }
    } else {
      body.status = '1';
    }
    if (!body.isContinuousVisit) {
      body.visitStrTime =
        moment(body.visitEndTime).format('YYYY-MM-DD') + ' 00:00:00';
    }
    try {
      const visitUserData = await ctx.model.YVisitUser.findOne({
        where: params,
      });
      const data = await visitUserData.update(body, {
        where: { ...params, transaction: t },
      });
      const visitBody = {
        statusName: body.status === '1' ? '已审批' : '待审批',
        createUser_Id: userStore.id,
        visit_Id: visitUserData.visitId,
        describe: '修改访客',
      };
      await ctx.model.YVisitState.create(visitBody, { transaction: t });
      const userIdList = await ctx.model.YSonVisitUser.findAll({
        where: { visit_id: visitUserData.visitId },
      });
      await userIdList.forEach(async item => {
        await ctx.model.YSonVisitUser.destroy({
          where: {
            visit_id: item.visit_id,
          },
          transaction: t,
        });
      });
      const visitData = [];
      await body.visitList.forEach(item => {
        item.visit_id = visitUserData.visitId;
        visitData.push(item);
      });
      await ctx.model.YSonVisitUser.bulkCreate(visitData, { transaction: t });

      await t.commit();
      this.success(data);
    } catch (error) {
      await t.rollback();
      return this.badRequest(`${error.message}`);
    }
  }
  /**
   * 获取今日访客人员
   */
  async getStaffVisit() {
    const { ctx } = this;
    const options = {
      where: {
        status: { [Op.in]: [ '4' ] },
        visitStrTime: { [Op.gte]: moment().format('YYYY-MM-DD') + ' 00:00:00' },
        visitEndTime: { [Op.lte]: moment().format('YYYY-MM-DD') + ' 23:59:59' },
      },
      include: [
        { model: ctx.model.YSonVisitUser, as: 'children', attributes: [ 'id' ] },
        {
          model: ctx.model.YVisitState,
          as: 'visitState',
          where: { statusName: '已到访', created_time: { [Op.gte]: moment().format('YYYY-MM-DD') + ' 00:00:00', [Op.lte]: moment().format('YYYY-MM-DD') + ' 23:59:59' } },
          attributes: [ 'created_time' ],
          order: [[ 'created_time', 'DESC' ]],
        },
      ],
      attributes: [ 'id', 'name', 'visitUnit', 'visitId', 'visitStrTime' ],
      order: [[ 'visitStrTime', 'DESC' ]],
      distinct: true,
    };
    const visitList = await ctx.model.YVisitUser.findAll(options);
    const data = [];
    visitList.forEach(element => {
      data.push({
        name: element.name,
        from: element.visitUnit,
        time: moment(element.visitState[0].dataValues.created_time).format(
          'HH:mm'
        ),
        type: element.children.length || '0',
        imgurl: '',
      });
    });
    this.success(data);
  }
  async visitorReport() {
    const { ctx } = this;
    const { query } = ctx;
    if (_.has(query, 'page_no')) {
      query.page_no = parseInt(query.page_no);
    }
    if (_.has(query, 'page_size')) {
      query.page_size = parseInt(query.page_size);
    }
    const rules = {
      page_no: { type: 'int', required: false, min: 1 },
      page_size: { type: 'int', required: false, min: 1 },
    };
    try {
      ctx.validate(rules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(
        `${error.message}: ${error.errors[0].code} ${error.errors[0].field}`
      );
    }
    const page_no = query.page_no || 1;
    const options = {
      where: { status: { [Op.ne]: '6' } },
      include: [
        {
          model: ctx.model.YAdminUser,
          as: 'adminUser',
          attributes: [
            'id',
            'name',
            'gender',
            'phone',
            'jobNum',
            'department',
            'isApproval',
            'role',
          ],
          required: false,
        },
        {
          model: ctx.model.YAdminUser,
          as: 'approverUser',
          attributes: [
            'id',
            'name',
            'gender',
            'phone',
            'jobNum',
            'department',
            'isApproval',
            'role',
          ],
          required: false,
        },
        {
          model: ctx.model.YSonVisitUser,
          as: 'children',
          attributes: [
            'id',
            'name',
            'idNum',
            'licensePlateNum',
            'healthCode',
            'tourCode',
            'visit_id',
          ],
          required: false,
        },
      ],
      attributes: [
        'id',
        'visitId',
        'name',
        'idNum',
        'address',
        'visitStrTime',
        'visitEndTime',
        'status',
        'visitUnit',
        'licensePlateNum',
        'healthCode',
        'tourCode',
        'describe',
        'createUser_Id',
        'approver_Id',
        'estimatedStrTime',
        'estimatedEndTime',
        'rejectDescribe',
        'actualVisit',
        'remark',
        'created_time',
        'isContinuousVisit',
        'isOutboundFreight',
        'phone',
      ],
      order: [[ 'created_time', 'DESC' ]],
    };
    if (_.has(query, 'name') && !_.isEmpty(query.name)) {
      options.where.name = { [Op.like]: '%' + query.name + '%' };
    }
    if (_.has(query, 'status') && !_.isEmpty(query.status)) {
      options.where.status = query.status;
    }
    if (_.has(query, 'id') && !_.isEmpty(query.id)) {
      options.where.visitId = { [Op.like]: '%' + query.id + '%' };
    }
    if (_.has(query, 'dateVisitEnd') && !_.isEmpty(query.dateVisitEnd)) {
      options.where[Op.or] = [
        {
          [Op.and]: [
            {
              visitStrTime: {
                [Op.gte]: query.dateVisitStr,
              },
            },
            {
              visitEndTime: {
                [Op.lte]: query.dateVisitEnd,
              },
            },
          ],
        },
        {
          [Op.and]: [
            {
              visitStrTime: {
                [Op.lte]: query.dateVisitEnd,
              },
            },
            {
              visitEndTime: {
                [Op.gte]: query.dateVisitStr,
              },
            },
          ],
        },
        {
          [Op.and]: [
            {
              visitStrTime: {
                [Op.lte]: query.dateVisitStr,
              },
            },
            {
              visitEndTime: {
                [Op.gte]: query.dateVisitStr,
              },
            },
          ],
        },
      ];
    }
    if (_.has(query, 'page_no') && _.has(query, 'page_size')) {
      options.offset = (query.page_no - 1) * query.page_size;
      options.limit = query.page_size;
    }
    const data = await ctx.model.YVisitUser.findAndCountAll(options);
    data.page_no = page_no;
    this.success(data);
  }
  /**
   * 用户列表
   */
  async index() {
    const { ctx } = this;
    const { query } = ctx;
    if (_.has(query, 'page_no')) {
      query.page_no = parseInt(query.page_no);
    }
    if (_.has(query, 'page_size')) {
      query.page_size = parseInt(query.page_size);
    }
    const rules = {
      page_no: { type: 'int', required: false, min: 1 },
      page_size: { type: 'int', required: false, min: 1 },
    };
    try {
      ctx.validate(rules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(
        `${error.message}: ${error.errors[0].code} ${error.errors[0].field}`
      );
    }
    const page_no = query.page_no || 1;
    const paramsA = {};
    const options = {
      where: { status: { [Op.ne]: '6' } },
      include: [
        {
          model: ctx.model.YAdminUser,
          as: 'adminUser',
          attributes: [
            'id',
            'name',
            'gender',
            'phone',
            'jobNum',
            'department',
            'isApproval',
            'role',
          ],
          required: false,
        },
        {
          model: ctx.model.YAdminUser,
          as: 'approverUser',
          attributes: [
            'id',
            'name',
            'gender',
            'phone',
            'jobNum',
            'department',
            'isApproval',
            'role',
          ],
          required: false,
        },
        {
          model: ctx.model.YSonVisitUser,
          as: 'children',
          attributes: [
            'id',
            'name',
            'idNum',
            'licensePlateNum',
            'healthCode',
            'tourCode',
            'visit_id',
          ],
          required: false,
        },
      ],
      attributes: [
        'id',
        'visitId',
        'name',
        'idNum',
        'address',
        'visitStrTime',
        'visitEndTime',
        'status',
        'visitUnit',
        'licensePlateNum',
        'healthCode',
        'tourCode',
        'describe',
        'createUser_Id',
        'approver_Id',
        'estimatedStrTime',
        'estimatedEndTime',
        'rejectDescribe',
        'actualVisit',
        'remark',
        'created_time',
        'isContinuousVisit',
        'isOutboundFreight',
        'phone',
      ],
      order: [[ 'created_time', 'DESC' ]],
    };
    if (_.has(query, 'name') && !_.isEmpty(query.name)) {
      options.where.name = { [Op.like]: '%' + query.name + '%' };
    }
    if (_.has(query, 'status') && !_.isEmpty(query.status)) {
      options.where.status = query.status;
    }
    const userStore = ctx.session.app_user;

    if (
      _.has(query, 'creator') &&
      !_.isEmpty(query.creator) &&
      userStore.role !== '0' &&
      userStore.role !== '1'
    ) {
      paramsA.name = { [Op.like]: '%' + query.creator + '%' };
      const adminUserList = await ctx.model.YAdminUser.findAll({
        where: paramsA,
      });
      const userIdList = await _.map(adminUserList, 'id');
      options.where.createUser_Id = { [Op.in]: userIdList };
    }

    if (_.has(query, 'id') && !_.isEmpty(query.id)) {
      options.where.visitId = { [Op.like]: '%' + query.id + '%' };
    }
    if (_.has(query, 'dateVisitEnd') && !_.isEmpty(query.dateVisitEnd)) {
      options.where[Op.or] = [
        {
          [Op.and]: [
            {
              visitStrTime: {
                [Op.gte]: query.dateVisitStr,
              },
            },
            {
              visitEndTime: {
                [Op.lte]: query.dateVisitEnd,
              },
            },
          ],
        },
        {
          [Op.and]: [
            {
              visitStrTime: {
                [Op.lte]: query.dateVisitEnd,
              },
            },
            {
              visitEndTime: {
                [Op.gte]: query.dateVisitStr,
              },
            },
          ],
        },
        {
          [Op.and]: [
            {
              visitStrTime: {
                [Op.lte]: query.dateVisitStr,
              },
            },
            {
              visitEndTime: {
                [Op.gte]: query.dateVisitStr,
              },
            },
          ],
        },
      ];
    }
    if (_.has(query, 'page_no') && _.has(query, 'page_size')) {
      options.offset = (query.page_no - 1) * query.page_size;
      options.limit = query.page_size;
    }
    if (userStore.role === '0') {
      options.where.createUser_Id = userStore.id;
    }
    if (userStore.role === '1') {
      options.where = {
        [Op.or]: [
          { createUser_Id: userStore.id },
          { approver_Id: userStore.id },
        ],
      };
    }
    const data = await ctx.model.YVisitUser.findAndCountAll(options);
    data.page_no = page_no;
    this.success(data);
  }

  // 修改访客单的状态（撤销，删除）
  async updateStatus() {
    const { ctx } = this;
    const { params } = ctx;
    const { body } = ctx.request;
    const bodyRules = {
      status: { type: 'string', required: true, allowEmpty: false },
      rejectDescribe: { type: 'string', required: false, allowEmpty: true },
      remark: { type: 'string', required: false, allowEmpty: true },
    };
    const paramRules = {
      visitId: { type: 'string', required: true },
      id: { type: 'string', required: true },
    };
    try {
      ctx.validate(paramRules, params);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(
        `${error.message}: ${error.errors[0].code} ${error.errors[0].field}`
      );
    }
    try {
      ctx.validate(bodyRules, body);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(
        `${error.message}: ${error.errors[0].code} ${error.errors[0].field}`
      );
    }
    let statusName;
    let transaction;
    const userStore = ctx.session.app_user;
    const visitBody = {
      statusName,
      createUser_Id: userStore.id,
    };
    const visitInfo = await ctx.model.YVisitUser.findOne({
      where: { id: params.id },
    });
    if (body.status === '0') {
      statusName = '待审批';
      visitBody.describe = '待审批';
    } else if (body.status === '1') {
      statusName = '已审批';
      visitBody.describe = '已审批';
    } else if (body.status === '2') {
      statusName = '已接待';
      visitBody.describe = '已接待';
    } else if (body.status === '3') {
      statusName = '待审批';
      visitBody.describe = '待审批';
    } else if (body.status === '4') {
      if (visitInfo.visitEndTime < new Date()) {
        const alarmParams = {
          cikey: '门岗',
          severity: 4,
          summary: '当前用户到访日期小于当前日期，不允许到访',
          alarmPerson: '5',
          subject: '门岗告警',
          text: '一条门岗告警',
        };
        await ctx.service.alarm.sendAlarm(alarmParams);
        return this.badRequest('当前用户到访日期小于当前日期，不允许到访');
      }
      const transporter = nodemailer.createTransport({
        service: 'qq',
        auth: {
          user: 'adsw-xxfs@qq.com', // generated ethereal user
          pass: 'rtbbnyejufamdggd', // generated ethereal password
        },
      });

      const userList = await ctx.model.YAdminUser.findAll({
        where: {
          id: visitInfo.createUser_Id,
        },
      });
      const emailList = await _.map(userList, 'email');
      // send mail with defined transport object
      await transporter.sendMail({
        from: 'adsw-xxfs@qq.com', // sender address
        to: emailList.toString() || 'adsw-xxfs@qq.com', // list of receivers
        subject: '访客到达信息', // Subject line
        text: visitInfo.name + '已到访', // plain text body
        html: `<b>${
          visitInfo.name + '已到，请到' + visitInfo.address + '接待' || ''
        }</b>`, // html body
      });

      statusName = '已到访';
      visitBody.describe = '已到访';
    } else if (body.status === '5') {
      statusName = '已离开';
      visitBody.describe = '已离开';
    } else {
      statusName = '删除';
      visitBody.describe = '删除';
    }
    visitBody.statusName = statusName;
    body.updated_time = new Date();
    try {
      transaction = await this.ctx.model.transaction();
      const data = await visitInfo.update(body, { where: { id: params.id } });
      visitBody.visit_Id = visitInfo.visitId;
      await ctx.model.YVisitState.create(visitBody);
      await transaction.commit();
      this.success(data);
    } catch (error) {
      await transaction.rollback();
      return this.badRequest(`${error.message}`);
    }
  }
}

module.exports = YVisitController;
