/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-28 20:57:08
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-03-30 13:48:01
 * @FilePath: \garden_service\app\controller\son_visit.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const BaseController = require('./base');

class YHumitureController extends BaseController {
  /**
   * 温湿度列表
   */
  async index() {
    const { ctx } = this;
    const options = {
      attributes: [ 'weizhi', 'wendu', 'shidu' ],
    };
    const HumidityData = await ctx.environmentModel.TemperatureHumidity.findAll(options);
    const data = [];
    HumidityData.forEach(item => {
      data.push([ item.weizhi, item.wendu, item.shidu ]);
    });
    this.success(data);
  }
  /**
   * 温湿度详情
   */
  async details() {
    const { ctx } = this;
    const options = {
      attributes: [ 'weizhi', 'wendu', 'shidu' ],
    };
    const humidityData = {};
    const data = await ctx.environmentModel.TemperatureHumidity.findAll(options);
    humidityData.titleName = [ '位置', '温度', '湿度' ];
    const valueAry = [];
    data.forEach(element => {
      valueAry.push(Object.values(element.dataValues));
    });
    humidityData.rowData = valueAry;
    this.success(humidityData);
  }
}

module.exports = YHumitureController;
