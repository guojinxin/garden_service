/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-26 09:45:59
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-04-06 09:51:38
 * @FilePath: \garden_service\app\controller\conferenceRoom.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const BaseController = require('./base');
const _ = require('lodash');
const moment = require('moment');
class ConferenceRoomController extends BaseController {
  async index() {
    const getTokenRes = await this.ctx.curl('http://10.34.5.20:3080/meeting/api/oapi/gettoken', {
      method: 'GET',
      data: {
        appkey: '1000ew0830bbf1xf82fu',
        appsecret: '29df2726443ed561dd85c958f69f935f8bd9120b',
      },
      contentType: 'json',
      dataType: 'json',
    }
    );
    const roomRes = await this.ctx.curl(`http://10.34.5.20:3080/meeting/api/open/room/list?access_token=${getTokenRes.data.value.access_token}`, {
      method: 'POST',
      contentType: 'json',
      dataType: 'json',
    }
    );
    const meetingRes = await this.ctx.curl(`http://10.34.5.20:3080/meeting/api/open/meeting/list?access_token=${getTokenRes.data.value.access_token}`, {
      method: 'POST',
      contentType: 'json',
      dataType: 'json',
      data: {
        order_by_expression: 'beginTime',
        direction: 'desc',
      },
    }
    );
    const data = [];
    meetingRes.data.value.forEach(element => {
      data.push([
        moment(element.beginTime).format('YYYY-MM-DD') + ' ' + moment(element.beginTime).format('HH:mm') + '-' + moment(element.endTime).format('HH:mm'),
        roomRes.data.value.find(item => item.id === element.roomId).name,
        element.speaker,
      ]);
    }
    );
    this.success(data);
  }
  async currentConference() {
    const getTokenRes = await this.ctx.curl('http://10.34.5.20:3080/meeting/api/oapi/gettoken', {
      method: 'GET',
      data: {
        appkey: '1000ew0830bbf1xf82fu',
        appsecret: '29df2726443ed561dd85c958f69f935f8bd9120b',
      },
      contentType: 'json',
      dataType: 'json',
    }
    );
    const roomRes = await this.ctx.curl(`http://10.34.5.20:3080/meeting/api/open/room/list?access_token=${getTokenRes.data.value.access_token}`, {
      method: 'POST',
      contentType: 'json',
      dataType: 'json',
    }
    );
    const remainder = 30 - (moment().minute() % 30);

    const dateTime = moment().add(remainder, 'minutes').format('YYYY-MM-DD HH:mm');
    const meetingRes = await this.ctx.curl(`http://10.34.5.20:3080/meeting/api/open/meeting/list?access_token=${getTokenRes.data.value.access_token}`, {
      method: 'POST',
      data: {
        createdBeginTime: moment().add(-(moment().minute() % 30), 'minutes').format('YYYY-MM-DD HH:mm'),
        createdEndTime: dateTime,
        order_by_expression: 'beginTime',
        direction: 'desc',
      },
      contentType: 'json',
      dataType: 'json',
    }
    );
    const data = [];
    meetingRes.data.value.forEach(element => {
      data.push([
        moment(element.beginTime).format('YYYY-MM-DD') + ' ' + moment(element.beginTime).format('HH:mm') + '-' + moment(element.endTime).format('HH:mm'),
        roomRes.data.value.find(item => item.id === element.roomId).name,
        element.speaker,
      ]);
    }
    );
    this.success(data);
  }
  async statistics() {
    const uinoData = [{
      roomId: '8A8AC9716894461DB80BCD3550762837',
      name: 'ZHL-2-会议室',
    }, {
      roomId: '95EFD94078EA4008B221CDA809945CE4',
      name: 'ZHL-1-党建室',
    }, {
      roomId: '7A295251896D476A9624B026A19309F1',
      name: 'ZHL-3-会议室',
    }, {
      roomId: '68F6B96CBC2246FCAE895E09686CA2E8',
      name: 'ZHL-1-接待会议室',
    }, {
      roomId: '5A26C0856B6841A78FC211A955F46FBA',
      name: 'ZHL-4-培训室',
    }, {
      roomId: '9F0B1E1F9A9543BBA050FADD5D95DF20',
      name: 'ZHL-4-会议室',
    }, {
      roomId: '389264B4AEDE4A48BE014BD7030FBDCB',
      name: 'ZHL-2-西会议室',
    }, {
      roomId: 'E491769C30C14768A3B2610B744F0EC8',
      name: 'ZHL-2-东会议室',
    }, {
      roomId: '389264B4AEDE4A48BE014BD7030FBDCB',
      name: 'YYCJ-3-东会议室',
    }];
    const getTokenRes = await this.ctx.curl('http://10.34.5.20:3080/meeting/api/oapi/gettoken', {
      method: 'GET',
      data: {
        appkey: '1000ew0830bbf1xf82fu',
        appsecret: '29df2726443ed561dd85c958f69f935f8bd9120b',
      },
      contentType: 'json',
      dataType: 'json',
    }
    );
    const remainder = 30 - (moment().minute() % 30);

    const dateTime = moment().add(remainder, 'minutes').format('YYYY-MM-DD HH:mm');
    const roomRes = await this.ctx.curl(`http://10.34.5.20:3080/meeting/api/open/room/list?access_token=${getTokenRes.data.value.access_token}`, {
      method: 'POST',
      contentType: 'json',
      dataType: 'json',
    }
    );
    const meetingRes = await this.ctx.curl(`http://10.34.5.20:3080/meeting/api/open/meeting/list?access_token=${getTokenRes.data.value.access_token}`, {
      method: 'POST',
      data: {
        createdBeginTime: moment().add(-(moment().minute() % 30), 'minutes').format('YYYY-MM-DD HH:mm'),
        createdEndTime: dateTime,
        order_by_expression: 'beginTime',
        direction: 'desc',
      },
      contentType: 'json',
      dataType: 'json',
    }
    );
    const uinoPerformance = [];
    _.forEach(uinoData, itemTwo => {
      if (_.isEmpty(meetingRes.data.value)) {
        uinoPerformance.push({
          cikey: itemTwo.name,
          kpi_name: '会议室使用情况',
          val: '关闭',
        });
      }
      _.forEach(meetingRes.data.value, item => {
        if (item.roomId === itemTwo.roomId) {
          uinoPerformance.push({
            cikey: itemTwo.name,
            kpi_name: '会议室使用情况',
            val: '开启',
          });
        } else {
          uinoPerformance.push({
            cikey: itemTwo.name,
            kpi_name: '会议室使用情况',
            val: '关闭',
          });
        }
      });
    });
    const data = await _.uniq(uinoPerformance);
    await this.ctx.curl(`${this.config.uinoPath + '/thing/dix/monitor/performance'}`, { method: 'POST', data, contentType: 'json', dataType: 'json' });
    this.success({
      key1: roomRes.data.value.length || '0',
      key2: meetingRes.data.value.length || '0',
      key3: (roomRes.data.value.length - meetingRes.data.value.length) < 0 ? 0 : roomRes.data.value.length - meetingRes.data.value.length,
      key4: [{ accountedFor: [ Number((meetingRes.data.value.length / roomRes.data.value.length).toFixed(2)), Number((meetingRes.data.value.length / roomRes.data.value.length + 0.05).toFixed(2)) ] }],
    });
  }
}

module.exports = ConferenceRoomController;
