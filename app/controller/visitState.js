/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-09-09 09:42:40
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-09-09 10:06:50
 * @FilePath: \garden_service\app\controller\visitState.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const BaseController = require('./base');

class YVisitStateController extends BaseController {
/**
   * 查看操作记录
   */
  async index() {
    const { ctx } = this;
    const { query } = ctx;
    const rules = {
      visitId: { type: 'string', required: true, allowEmpty: false },
    };
    try {
      ctx.validate(rules, query);
    } catch (error) {
      ctx.logger.info(error);
      return this.badRequest(`${error.message}: ${error.errors[0].code} ${error.errors[0].field}`);
    }
    const options = {
      where: { visit_Id: query.visitId },
      include: [{ model: ctx.model.YAdminUser, as: 'adminUser', attributes: [ 'id', 'name', 'gender', 'phone', 'jobNum', 'department', 'isApproval', 'role' ], required: false }],
      attributes: [ 'id', 'statusName', 'createUser_Id', 'visit_Id', 'describe', 'created_time' ],
      order: [[ 'created_time', 'DESC' ]],
    };
    const data = await ctx.model.YVisitState.findAll(options);
    this.success(data);
  }

}

module.exports = YVisitStateController;
