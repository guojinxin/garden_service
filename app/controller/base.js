/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-23 09:40:58
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-08-31 10:56:27
 * @FilePath: \garden_service\app\controller\base.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const Controller = require('egg').Controller;
const _ = require('lodash');

class BaseController extends Controller {
  /**
   * 成功响应
   * @param {*} data 业务数据
   */
  success(data) {
    if (!_.isObject(data)) data = { message: data };
    this.ctx.body = { message: 'OK', code: 0, status: 200, data };
  }

  /**
   * Returns a 400 Bad Request error
   * @param {*} msg 响应信息
   * @param {*} code 响应错误码
   */
  badRequest(msg, code = -1) {
    msg = msg || 'bad request';
    this.ctx.status = 200;
    this.ctx.body = { code: code || -1, status: 400, message: msg };
    return;
  }

  /**
   * Returns a 401 Unauthorized error
   * @param {*} msg message
   */
  unauthorized(msg) {
    this.ctx.status = 200;
    this.ctx.body = { code: -100, status: 401, message: msg };
    return;
  }
}

module.exports = BaseController;
