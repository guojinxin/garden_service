/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-26 14:44:39
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-02-07 13:41:48
 * @FilePath: \garden_service\app\attendance_model\employee.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';

/**
 * Parked
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { STRING, INTEGER } = app.Sequelize;
  const Employee = app.attendanceMssql.define('kqz_employee', {
    EmployeeID: {
      type: STRING(512),
      primaryKey: true,
      autoIncrement: true,
    },
    BrchID: {
      type: INTEGER(8),
    },
    EmployeeName: {
      type: STRING,
    },
  }, {
    freezeTableName: true,
    tableName: 'kqz_employee',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
  });
  Employee.associate = function() {
    app.attendanceMssql.Employee.hasMany(app.attendanceMssql.Card, { as: 'card', foreignKey: 'EmployeeID' });
    app.attendanceMssql.Employee.belongsTo(app.attendanceMssql.Brch, { as: 'brch', foreignKey: 'BrchID', targetKey: 'BrchID' });

  };
  return Employee;
};
