/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-26 14:44:39
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-06-07 19:08:27
 * @FilePath: \garden_service\app\attendance_model\employee.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const moment = require('moment');

/**
 * KQZ_Card
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { INTEGER, DATE } = app.Sequelize;
  const Card = app.attendanceMssql.define('kqz_card', {
    CardID: {
      type: INTEGER(8),
      primaryKey: true,
      autoIncrement: true,
    },
    EmployeeID: {
      type: INTEGER(8),
    },
    CardTime: {
      type: DATE,
      get() {
        return moment(this.getDataValue('CardTime')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
  }, {
    freezeTableName: true,
    tableName: 'kqz_card',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
  });
  Card.associate = function() {
    app.attendanceMssql.Card.belongsTo(app.attendanceMssql.Employee, { as: 'employee', foreignKey: 'EmployeeID', targetKey: 'EmployeeID' });

  };
  return Card;
};
