/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2023-01-17 17:11:34
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-01-18 10:19:44
 * @FilePath: \garden_service\app\attendance_model\brch.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

'use strict';

/**
 * kqz_brch
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { INTEGER, STRING } = app.Sequelize;
  const Brch = app.attendanceMssql.define('kqz_brch', {
    BrchID: {
      type: INTEGER(8),
      primaryKey: true,
      autoIncrement: true,
    },
    BrchName: {
      type: STRING(50),
    },
  }, {
    freezeTableName: true,
    tableName: 'kqz_brch',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
  });
  Brch.associate = function() {
    app.attendanceMssql.Brch.hasMany(app.attendanceMssql.Employee, { as: 'employee', foreignKey: 'BrchID' });
  };
  return Brch;
};
