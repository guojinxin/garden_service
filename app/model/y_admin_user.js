/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-16 13:41:11
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-07-13 17:11:16
 * @FilePath: \garden_service\app\model\y_admin_user.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const uuid = require('uuid');

/**
 * y_admin_user 用户表
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { STRING, BOOLEAN, UUID, ENUM, JSON } = app.Sequelize;
  const YAdminUser = app.model.define('y_admin_user', {
    id: {
      type: UUID,
      primaryKey: true,
      defaultValue: () => {
        return uuid.v1();
      },
    },
    nickname: {
      type: STRING(20), allowNull: false,
      unique: true, comments: '用户名',
    },
    password: {
      type: STRING(50), allowNull: false,
      unique: true, comments: '密码',
    },
    name: {
      type: STRING(50),
      allowNull: true,
      comments: '姓名',
    },
    gender: {
      type: ENUM,
      allowNull: true,
      values: [ '0', '1' ], // 0男，1女
      comments: '性别',
    },
    phone: {
      type: STRING(50),
      comments: '手机号',
    },
    email: {
      type: STRING(50),
      comments: '邮箱',
    },
    jobNum: {
      type: STRING(50),
      comments: '工号',
    },
    department: {
      type: STRING(50),
      comments: '部门',
    },
    isApproval: {
      type: BOOLEAN,
      comments: '审批权限',
      defaultValue: true,
    },
    role: {
      type: ENUM,
      allowNull: true,
      values: [ '0', '1', '2', '3' ], // 0普通，1审批，2管理 3保安
      comments: '角色',
    },
    isDelete: {
      type: BOOLEAN,
      comments: '是否删除',
      defaultValue: false,
    },
    isAlarm: {
      type: BOOLEAN,
      comments: '是否告警权限',
      defaultValue: false,
    },
    alarmPerson: {
      type: JSON,
      allowNull: true,
      values: [ '1', '2', '3', '4', '5', '6', '7', '8' ], // 1污水检测指标告警负责人 2水电处理人 3室内温度指标告警负责人 4消防告警负责人 5门岗告警负责人 6温湿度告警负责人 7门禁告警负责人 8 监控告警负责人
      comments: '告警类型',
    },
    operationPermission: {
      type: JSON,
      comments: '1:水表管控 2:电表管控 3:汽表管控 4:考勤报表 5道闸报表 6门禁报表 7巡更报表 8访客报表',
    },
  }, {
    freezeTableName: true,
    tableName: 'y_admin_user',
    timestamps: true,
    createdAt: 'created_time',
    updatedAt: false,
  });

  return YAdminUser;
};
