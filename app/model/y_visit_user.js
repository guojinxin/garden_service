/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-05 17:04:17
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-07-11 13:59:31
 * @FilePath: \garden_service\app\model\y_visit_user.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const uuid = require('uuid');
const moment = require('moment');

/**
 * y_visit_user
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { STRING, DATE, ENUM, UUID, BOOLEAN, JSON } = app.Sequelize;
  const YVisitUser = app.model.define('y_visit_user', {
    id: {
      type: STRING,
      primaryKey: true,
      comments: '主键id',
    },
    visitId: {
      type: UUID,
      defaultValue: () => {
        return uuid.v1();
      },
    },
    isContinuousVisit: {
      type: BOOLEAN,
      defaultValue: false,
      comments: '连续访问',
    },
    isOutboundFreight: {
      type: BOOLEAN,
      defaultValue: false,
      comments: '是否运出货物',
    },
    outboundFreightImage: {
      type: JSON,
      comments: '货物照片',
    },
    name: {
      type: STRING(50),
      allowNull: true,
      comments: '姓名',
    },
    idNum: {
      type: STRING(50),
      comments: '身份证号码',
    },
    phone: {
      type: STRING(50),
      comments: '手机号',
    },
    visitStrTime: {
      type: DATE,
      comments: '拜访时间（开始时间）',
    },
    visitEndTime: {
      type: DATE,
      comments: '拜访时间（结束时间）',
    },
    status: {
      type: ENUM,
      allowNull: true,
      values: [ '0', '1', '2', '3', '4', '5', '6' ], // 0待审批，1已审批，2已接待，3待审批（查看信息），4已到访 5已离开 6删除
      comments: '状态',
      defaultValue: '0',
    },
    address: {
      type: STRING(50),
      comments: '接访地点',
    },
    visitUnit: {
      type: STRING(50),
      comments: '来访单位',
    },
    licensePlateNum: {
      type: STRING(200),
      comments: '车牌号码',
    },
    healthCode: {
      type: STRING(200),
      comments: '健康码',
    },
    tourCode: {
      type: STRING(200),
      comments: '行程码',
    },
    describe: {
      type: STRING(300),
      comments: '拜访事由',
    },
    createUser_Id: {
      type: STRING,
      comments: '申请人id',
    },
    approver_Id: {
      type: STRING,
      comments: '审批人id',
    },
    estimatedStrTime: {
      type: DATE,
      comments: '预计到达时间',
    },
    estimatedEndTime: {
      type: DATE,
      comments: '预计结束时间',
    },
    rejectDescribe: {
      type: STRING(200),
      comments: '驳回理由',
    },
    actualVisit: {
      type: STRING(200),
      comments: '实际来访情况',
    },
    remark: {
      type: STRING(200),
      comments: '备注',
    },
    updated_time: {
      type: DATE,
      allowNull: true,
      comments: '修改时间',
      get() {
        return moment(this.getDataValue('updated_time')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
  }, {
    freezeTableName: true,
    tableName: 'y_visit_user',
    timestamps: true,
    createdAt: 'created_time',
    updatedAt: 'updated_time',
  });
  YVisitUser.associate = function() {
    app.model.YVisitUser.belongsTo(app.model.YAdminUser, { as: 'adminUser', foreignKey: 'createUser_Id' });
    app.model.YVisitUser.belongsTo(app.model.YAdminUser, { as: 'approverUser', foreignKey: 'approver_Id' });
    app.model.YVisitUser.hasMany(app.model.YSonVisitUser, { as: 'children', foreignKey: 'visit_id', sourceKey: 'visitId' });
    app.model.YVisitUser.hasMany(app.model.YVisitState, { as: 'visitState', foreignKey: 'visit_id', sourceKey: 'visitId' });
  };
  return YVisitUser;
};
