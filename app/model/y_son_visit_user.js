/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-05 17:04:17
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-08-29 20:22:08
 * @FilePath: \garden_service\app\model\y_visit_user.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const uuid = require('uuid');

/**
 * y_visit_user
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { INTEGER, STRING, UUID } = app.Sequelize;
  const YSonVisitUser = app.model.define('y_son_visit_user', {
    id: {
      type: UUID,
      primaryKey: true,
      defaultValue: () => {
        return uuid.v1();
      },
    },
    name: {
      type: STRING(50),
      allowNull: true,
      comments: '姓名',
    },
    idNum: {
      type: INTEGER(18),
      comments: '身份证号码',
    },
    licensePlateNum: {
      type: STRING(200),
      comments: '车牌号码',
    },
    healthCode: {
      type: STRING(200),
      comments: '健康码',
    },
    tourCode: {
      type: STRING(200),
      comments: '行程码',
    },
    visit_id: {
      type: STRING,
      comments: '申请人id',
    },
  }, {
    freezeTableName: true,
    tableName: 'y_son_visit_user',
    timestamps: true,
    createdAt: 'created_time',
    updatedAt: 'updated_time',
  });

  return YSonVisitUser;
};
