/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-11-16 14:51:15
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-11-16 14:53:17
 * @FilePath: \garden_service\app\model\typeex.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

'use strict';

/**
 * typeex 类型表
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { STRING, INTEGER } = app.Sequelize;
  const Typeex = app.model.define('typeex', {
    ID: {
      type: INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    Type: {
      type: INTEGER,
      allowNull: false,
    },
    Name: {
      type: STRING(100),
      allowNull: true,
      comments: '姓名',
    },
  }, {
    freezeTableName: true,
    tableName: 'typeex',
    timestamps: true,
    createdAt: false,
    updatedAt: false,
  });
  return Typeex;
};
