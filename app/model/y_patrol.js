/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-01 16:53:44
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-06-14 14:47:05
 * @FilePath: \garden_service\app\model\y_alarm.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

'use strict';
const moment = require('moment');

/**
 * y_patrol
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { STRING, DATE } = app.Sequelize;
  const YPatrol = app.model.define('y_patrol', {
    checkpointId: {
      type: STRING,
      primaryKey: true,

      comments: '巡检点ID',
    },
    checkpointName: {
      type: STRING,
      comments: '巡检点名称',
    },
    checkpointCard: {
      type: STRING,
      comments: '巡检点卡号',
    },
    patrolmanId: {
      type: STRING,
      comments: '巡检员ID',
    },
    patrolmanName: {
      type: STRING,
      comments: '巡检员名称',
    },
    patrolmanCard: {
      type: STRING,
      comments: '巡检员卡号',
    },
    createTime: {
      type: DATE,
      comments: '生成时间',
      get() {
        return moment(this.getDataValue('createTime')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    deviceId: {
      type: STRING,
      comments: '巡检器ID',
    },
    deviceName: {
      type: STRING,
      comments: '巡检器名称',
    },
    deviceCode: {
      type: STRING,
      comments: '巡检器卡号',
    },
    areaId: {
      type: STRING,
      comments: '组织ID',
    },
    areaName: {
      type: STRING,
      comments: '组织名称',
    },
  }, {
    freezeTableName: true,
    tableName: 'y_patrol',
    timestamps: true,
    createdAt: 'createTime',
    updatedAt: false,
  });

  return YPatrol;
};
