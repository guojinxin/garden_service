'use strict';

/**
 * y_point
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { STRING, INTEGER } = app.Sequelize;
  const YPoint = app.model.define('y_point', {
    id: {
      type: INTEGER,
      primaryKey: true,
      comments: '巡检员ID',
    },
    name: {
      type: STRING,
      comments: '巡检员名称',
    },
    card: {
      type: STRING,
      comments: '巡检点卡号',
    },
    areaId: {
      type: STRING,
      comments: '巡检员ID',
    },
    areaName: {
      type: STRING,
      comments: '巡检员名称',
    },
  }, {
    freezeTableName: true,
    tableName: 'y_point',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
  });

  return YPoint;
};
