/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-05 17:04:17
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-11-15 11:24:55
 * @FilePath: \garden_service\app\model\y_visit_user.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const uuid = require('uuid');
const moment = require('moment');

/**
 * y_visit_user
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { STRING, UUID, DATE } = app.Sequelize;
  const YVisitState = app.model.define('y_visit_state', {
    id: {
      type: UUID,
      primaryKey: true,
      defaultValue: () => {
        return uuid.v1();
      },
    },
    statusName: {
      type: STRING(50),
      allowNull: true,
      comments: '状态',
    },
    createUser_Id: {
      type: STRING,
      comments: '创建人id',
    },
    visit_Id: {
      type: STRING,
      comments: '访客id',
    },
    describe: {
      type: STRING,
      comments: '描述',
    },
    created_time: {
      type: DATE,
      allowNull: true,
      comments: '显示时间',
      get() {
        return moment(this.getDataValue('created_time')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
  }, {
    freezeTableName: true,
    tableName: 'y_visit_state',
    timestamps: true,
    createdAt: 'created_time',
    updatedAt: false,
  });
  YVisitState.associate = function() {
    app.model.YVisitState.belongsTo(app.model.YAdminUser, { as: 'adminUser', foreignKey: 'createUser_Id' });
  };
  return YVisitState;
};
