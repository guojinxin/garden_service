/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-01 16:53:44
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-03-22 09:47:17
 * @FilePath: \garden_service\app\model\y_alarm.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

'use strict';
const uuid = require('uuid');
const moment = require('moment');

/**
 * y_dictionary_data
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { STRING, UUID, ENUM, DATE } = app.Sequelize;
  const YDictionaryData = app.model.define('y_dictionary_data', {
    id: {
      type: UUID,
      primaryKey: true,
      defaultValue: () => {
        return uuid.v1();
      },
    },
    type: {
      type: ENUM,
      allowNull: true,
      values: [ '0', '1', '2' ], // 0水表 1电表 2汽表
      comments: '类型',
      defaultValue: '0',
    },
    status: {
      type: ENUM,
      allowNull: true,
      values: [ '0', '1' ], // 0关闭 1启动
      defaultValue: '1',
      comments: '状态',
    },
    name: {
      type: STRING(300),
      allowNull: true,
      comments: '数据名称',
    },
    position: {
      type: STRING(300),
      allowNull: true,
      comments: '位置',
    },
    created_time: {
      type: DATE,
      allowNull: true,
      comments: '创建时间',
      get() {
        return moment(this.getDataValue('created_time')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    updated_time: {
      type: DATE,
      allowNull: true,
      comments: '修改时间',
      get() {
        return moment(this.getDataValue('updated_time')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
  }, {
    freezeTableName: true,
    tableName: 'y_dictionary_data',
    timestamps: true,
    createdAt: 'created_time',
    updatedAt: 'updated_time',
  });

  return YDictionaryData;
};
