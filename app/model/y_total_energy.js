/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-06 10:50:05
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-02-03 09:58:20
 * @FilePath: \garden_service\app\model\y_total_energy.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

'use strict';
const uuid = require('uuid');
const moment = require('moment');

/**
 * y_total_energy
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { UUID, DATE, ENUM, DOUBLE } = app.Sequelize;
  const YTotalEnergy = app.model.define('y_total_energy', {
    id: {
      type: UUID,
      primaryKey: true,
      defaultValue: () => {
        return uuid.v1();
      },
    },
    total: {
      type: DOUBLE,
      allowNull: true,
      comments: '总量',
    },
    type: {
      type: ENUM,
      allowNull: true,
      values: [ '0', '1', '2', '3' ], // 0水 1电 2汽 园区总人数
      comments: '状态',
      defaultValue: '0',
    },
    created_time: {
      type: DATE,
      allowNull: true,
      comments: '创建时间',
      get() {
        return moment(this.getDataValue('created_time')).format('YYYY-MM');
      },
    },
    updated_time: {
      type: DATE,
      allowNull: true,
      comments: '修改时间',
      get() {
        return moment(this.getDataValue('updated_time')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    amount_total: {
      type: DOUBLE,
      allowNull: true,
      comments: '金额总量',
    },
  }, {
    freezeTableName: true,
    tableName: 'y_total_energy',
    timestamps: true,
    createdAt: 'created_time',
    updatedAt: 'updated_time',
  });

  return YTotalEnergy;
};
