/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-01 16:53:44
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-03-28 15:05:54
 * @FilePath: \garden_service\app\model\y_alarm.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

'use strict';
const uuid = require('uuid');
const moment = require('moment');

/**
 * y_alarm
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { STRING, UUID, ENUM, DATE } = app.Sequelize;
  const YAlarm = app.model.define('y_alarm', {
    id: {
      type: UUID,
      primaryKey: true,
      defaultValue: () => {
        return uuid.v1();
      },
    },
    alarm_time: {
      type: DATE,
      allowNull: false,
      comments: '告警时间',
      get() {
        return moment(this.getDataValue('alarm_time')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    modify_time: {
      type: DATE,
      allowNull: false,
      comments: '修改时间',
      get() {
        return moment(this.getDataValue('modify_time')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    alarm_information: {
      type: STRING(300),
      allowNull: true,
      comments: '告警描述',
    },
    alarmlevel: {
      type: ENUM,
      allowNull: true,
      values: [ '1', '2', '3', '4' ], // 1严重 2重要 3次要 4普通
      comments: '告警级别',
    },
    status: {
      type: ENUM,
      allowNull: true,
      values: [ '1', '2', '3' ], // 1待处理 2处理中 3关闭
      comments: '状态',
    },
    ciName: {
      type: STRING,
      comments: '设备名称',
    },
    distributionUser_Id: {
      type: STRING,
      comments: '分配用户id',
    },
    handlingOpinions: {
      type: STRING,
      comments: '处理意见',
    },
    alarmType: {
      type: ENUM,
      allowNull: true,
      values: [ '1', '2', '3', '4', '5', '6' ], // 1污水检测指标告警 2水电 3室内温度指标告警 4消防告警 5门岗告警 6温湿度告警
      comments: '告警类型',
    },
  }, {
    freezeTableName: true,
    tableName: 'y_alarm',
    timestamps: true,
    createdAt: false,
    updatedAt: false,
  });
  YAlarm.associate = function() {
    app.model.YAlarm.belongsTo(app.model.YAdminUser, { as: 'adminUser', foreignKey: 'distributionUser_Id' });
  };
  return YAlarm;
};
