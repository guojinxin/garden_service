/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-05 17:16:14
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-12-09 18:07:05
 * @FilePath: \garden_service\app\model\y_use_water.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

'use strict';
const uuid = require('uuid');
const moment = require('moment');

/**
 * y_use_water
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { STRING, UUID, BOOLEAN, DATE, DOUBLE } = app.Sequelize;
  const YUseWater = app.model.define('y_use_water', {
    id: {
      type: UUID,
      primaryKey: true,
      defaultValue: () => {
        return uuid.v1();
      },
    },
    name: {
      type: STRING,
      allowNull: true,
      comments: '表号',
    },
    machineData: {
      type: DOUBLE,
      allowNull: false,
      comments: '机表数据',
      defaultValue: 0,
    },
    monthlyDosage: {
      type: DOUBLE,
      allowNull: false,
      comments: '本月用水量',
      defaultValue: 0,
    },
    position: {
      type: STRING(300),
      allowNull: true,
      comments: '位置',
    },
    isAutomatic: {
      type: BOOLEAN,
      comments: '是否自动采集',
      defaultValue: true,
    },
    isDelete: {
      type: BOOLEAN,
      allowNull: true,
      comments: '是否删除 0正常 1删除',
      defaultValue: false,
    },
    entry_time: {
      type: DATE,
      allowNull: true,
      comments: '统计日期',
      get() {
        return moment(this.getDataValue('entry_time')).format('YYYY-MM');
      },
    },
    created_time: {
      type: DATE,
      allowNull: true,
      comments: '创建时间',
      get() {
        return moment(this.getDataValue('created_time')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    updated_time: {
      type: DATE,
      allowNull: true,
      comments: '修改时间',
      get() {
        return moment(this.getDataValue('updated_time')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
  }, {
    freezeTableName: true,
    tableName: 'y_use_water',
    timestamps: true,
    createdAt: 'created_time',
    updatedAt: 'updated_time',
  });

  return YUseWater;
};
