/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-05 16:18:56
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-07-14 13:31:19
 * @FilePath: \garden_service\app\router.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
/**
 * App 端接口
 * @param {Egg.Application} app - egg application
 * @param {String} prefix - 路由公共前缀
 */
module.exports = app => {
  // app.beforeStart(async () => {
  //   await app.model.sync({ alter: true });//force  false 为不覆盖 true会删除再创建; alter true可以 添加或删除字段;  });
  // })
  const { router, controller, middleware } = app;
  // 引入
  const jwt = middleware.jwt(app.config.jwt);
  // admin
  router.get('/users/index', jwt, controller.users.index);
  router.get('/users/approverIndex', jwt, controller.users.approverIndex);
  router.post('/users/create', jwt, controller.users.create);
  router.post('/users/login', controller.users.login);
  // router.post('/users/testIndex', controller.users.testIndex);
  router.post('/users/editPwd', jwt, controller.users.editPwd);
  router.put('/users/resetPwd/:id', jwt, controller.users.resetPwd);
  router.put('/users/update/:id', jwt, controller.users.update);
  router.put('/users/updateApproval/:id', jwt, controller.users.updateApproval);
  router.delete('/users/destroy/:id', jwt, controller.users.destroy);
  router.put('/users/keyApproval/:isOpen', jwt, controller.users.keyApproval);
  // visit
  router.post('/visit/create', jwt, controller.visit.create);
  router.get('/visit/index', jwt, controller.visit.index);
  router.get('/visit/visitorReport', jwt, controller.visit.visitorReport);
  router.post('/visit/getStaffVisit', controller.visit.getStaffVisit);
  router.put('/visit/update/:visitId', jwt, controller.visit.update);
  // 修改货物照片
  router.put('/visit/updateOutboundFreightImage/:id', jwt, controller.visit.updateOutboundFreightImage);
  // 获取货物照片
  router.get('/visit/getOutboundFreightImage', jwt, controller.visit.getOutboundFreightImage);
  router.put('/visit/updateStatus/:id/:visitId', jwt, controller.visit.updateStatus);// 0待审批，1已审批，2已接待，3待审批（查看驳回信息），4已到访 5已离开 6删除
  // upload
  router.post('/users/uploadAvatar', jwt, controller.upload.uploadAvatar);
  // visit_son
  router.post('/son_visit/create', jwt, controller.sonVisit.create);
  router.put('/son_visit/update/:id', jwt, controller.sonVisit.update);
  router.delete('/son_visit/destroy/:id', jwt, controller.sonVisit.destroy);
  router.get('/sonVisit/index', jwt, controller.sonVisit.index);
  router.get('/sonVisit/visit_index', jwt, controller.sonVisit.visit_index);
  // visit_state
  router.get('/visitState/index', jwt, controller.visitState.index);
  // garden
  router.post('/garden/getWeather', controller.garden.getWeather);
  router.post('/garden/index', controller.garden.index);
  // video
  router.get('/video/index', controller.video.index);
  // parked
  router.post('/parked/index', controller.brake.index);
  router.post('/parked/parkedStatistics', controller.brake.parkedStatistics);
  router.post('/parked/parkedStatisticsLine', controller.brake.parkedStatisticsLine);
  // accessControl
  router.post('/parked/accessControl', controller.brake.accessControl);
  // park
  // 道闸部门
  router.get('/park/sluiceDepartment', jwt, controller.park.sluiceDepartment);
  // 道闸列表
  router.get('/sluice/list', jwt, controller.park.sluiceList);
  router.post('/park/index', controller.park.index);


  // alarm
  router.post('/alarm/handle', controller.alarm.handle);
  router.post('/alarm/sendAlarm', controller.alarm.sendAlarm);
  router.get('/alarm/index', jwt, controller.alarm.index);
  router.put('/alarm/updateStatus/:id', jwt, controller.alarm.updateStatus);
  router.post('/alarm/getAlarmDate', controller.alarm.getAlarmDate);
  router.post('/alarm/getAlarmStatistics', controller.alarm.getAlarmStatistics);
  // humiture
  router.post('/humiture/index', controller.humiture.index);
  router.post('/humiture/details', controller.humiture.details);

  // dictionaryData
  router.post('/dictionary/create', jwt, controller.dictData.create);
  router.put('/dictionary/update/:id', jwt, controller.dictData.update);
  router.get('/dictionary/index', jwt, controller.dictData.index);
  // y_use_water
  router.get('/useWater/index', jwt, controller.useWater.index);
  router.get('/useWater/getDictionaryData', jwt, controller.useWater.getDictionaryData);
  router.post('/useWater/create', jwt, controller.useWater.create);
  router.post('/useWater/waterStatistics', controller.useWater.waterStatistics);
  router.post('/useWater/getEnergyLine', controller.useWater.getEnergyLine);
  router.post('/useWater/getAmountEnergyLine', controller.useWater.getAmountEnergyLine);
  router.post('/useWater/getAutomaticWater', controller.useWater.getAutomaticWater);
  router.post('/useWater/getEnergyAll', controller.useWater.getEnergyAll);

  // y_use_steam
  router.get('/useSteam/index', jwt, controller.useSteam.index);
  router.post('/useSteam/create', jwt, controller.useSteam.create);
  router.post('/useSteam/steamStatistics', controller.useSteam.steamStatistics);

  // y_use_electricity
  router.get('/useElectricity/index', jwt, controller.useElectricity.index);
  router.post('/useElectricity/create', jwt, controller.useElectricity.create);
  router.post('/useElectricity/electricityStatistics', controller.useElectricity.electricityStatistics);
  // ConferenceRoom
  router.post('/conferenceRoom/index', controller.conferenceRoom.index);
  router.post('/conferenceRoom/statistics', controller.conferenceRoom.statistics);
  router.post('/conferenceRoom/currentConference', controller.conferenceRoom.currentConference);

  // Attendance
  router.post('/attendance/index', controller.attendance.index);
  router.post('/attendance/getAttendanceRate', controller.attendance.getAttendanceRate);
  // 部门考勤率
  router.post('/attendance/departmentAttendanceRate', controller.attendance.departmentAttendanceRate);
  router.post('/attendance/personnelAttendance', controller.attendance.personnelAttendance);
  // 考勤列表
  router.get('/attendance/list', jwt, controller.attendance.list);
  // 获取所有部门
  router.get('/attendance/getBrch', jwt, controller.attendance.getBrch);

  // EntranceGuard
  router.post('/entranceGuard/index', controller.entranceGuard.index);
  router.post('/entranceGuard/personnel', controller.entranceGuard.personnel);
  router.post('/entranceGuard/getEntranceGuardIndex', controller.entranceGuard.getEntranceGuardIndex);
  // 根据id查询巡更列表
  router.post('/entranceGuard/getPatrol', controller.entranceGuard.getPatrol);
  // 获取巡更列表
  router.post('/entranceGuard/getPatrolList', controller.entranceGuard.getPatrolList);
  // 查询门禁的部门
  router.get('/entranceGuard/accessControlDepartment', jwt, controller.entranceGuard.accessControlDepartment);
  // 获取门禁列表
  router.get('/entranceGuard/getDepartmentList', jwt, controller.entranceGuard.getDepartmentList);
  // 查看巡更点位
  router.get('/entranceGuard/getPoint', jwt, controller.entranceGuard.getPoint);
  // 管理 查看巡更列表
  router.get('/entranceGuard/getPatrolDataList', jwt, controller.entranceGuard.getPatrolDataList);
  // energyConsumption
  router.get('/energyConsumption/index', jwt, controller.energyConsumption.index);
  router.put('/energyConsumption/update/:id', jwt, controller.energyConsumption.update);

};
