'use strict';

/**
 * Netdevice 设备表
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { STRING, INTEGER } = app.Sequelize;
  const NetDevice = app.accessControlModel.define(
    'netdevice',
    {
      ID: {
        type: INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      IP: {
        type: STRING(50),
        allowNull: false,
      },
    },
    {
      freezeTableName: true,
      tableName: 'netdevice',
      timestamps: true,
      createdAt: 'CreateDateTime',
      updatedAt: 'ModifyDateTime',
    }
  );
  return NetDevice;
};
