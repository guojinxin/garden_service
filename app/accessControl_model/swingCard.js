/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-29 19:28:01
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-02-23 11:19:14
 * @FilePath: \garden_service\app\model\swingCard.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

'use strict';
const moment = require('moment');

/**
 * swingcard 门禁进出表
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { INTEGER, STRING, DATE } = app.Sequelize;
  const SwingCard = app.accessControlModel.define('swingcard', {
    ID: {
      type: INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    PersonnelID: {
      type: INTEGER,
      allowNull: false,
    },
    CreateDateTime: {
      type: DATE,
      allowNull: true,
      comments: '修改时间',
      get() {
        return moment(this.getDataValue('CreateDateTime')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    DevLocation: {
      type: STRING,
      allowNull: false,
    },
    DeviceID: {
      type: INTEGER,
      allowNull: false,
    },
  }, {
    freezeTableName: true,
    tableName: 'swingcard',
    timestamps: true,
    createdAt: false,
    updatedAt: false,
  });
  SwingCard.associate = function() {
    app.accessControlModel.SwingCard.belongsTo(app.accessControlModel.Personnel, { as: 'personnel', foreignKey: 'PersonnelID' });
  };
  return SwingCard;
};
