/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-16 13:41:11
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-06-06 19:35:31
 * @FilePath: \garden_service\app\model\y_admin_user.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';

/**
 * personnel 人员表
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { STRING, DATE, INTEGER, BLOB } = app.Sequelize;
  const Personnel = app.accessControlModel.define('personnel', {
    ID: {
      type: INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    ParentType: {
      type: INTEGER,
      allowNull: false,
    },
    ParentID: {
      type: INTEGER,
      allowNull: false,
    },
    Name: {
      type: STRING(100),
      allowNull: false,
      comments: '姓名',
    },
    Sex: {
      type: INTEGER,
      allowNull: false,
      comments: '性别',
    },
    PhoneNumber: {
      type: STRING(30),
      allowNull: false,
      comments: '手机号',
    },
    CredentialsType: {
      type: INTEGER,
      allowNull: false,
    },
    CredentialsNum: {
      type: STRING(50),
      allowNull: false,
    },
    Location: {
      type: STRING(80),
      allowNull: true,
    },
    Email: {
      type: STRING(100),
      allowNull: true,
    },
    Birthday: {
      type: DATE,
      allowNull: true,
    },
    Nationality: {
      type: INTEGER,
      allowNull: true,
    },
    NativePlace: {
      type: INTEGER,
      allowNull: true,
    },
    Nation: {
      type: INTEGER,
      allowNull: true,
    },
    PoliticalStatus: {
      type: INTEGER,
      allowNull: true,
    },
    Household: {
      type: INTEGER,
      allowNull: true,
    },
    RegisteredResidence: {
      type: STRING(30),
      allowNull: true,
    },
    IssuingAuthority: {
      type: STRING(30),
      allowNull: true,
    },
    Education: {
      type: INTEGER,
      allowNull: true,
    },
    AcademicDegree: {
      type: INTEGER,
      allowNull: true,
    },
    GraduateTime: {
      type: DATE,
      allowNull: true,
    },
    GraduateSchool: {
      type: STRING(30),
      allowNull: true,
    },
    UseCardPwd: {
      type: INTEGER,
      allowNull: true,
    },
    UseAttendance: {
      type: INTEGER,
      allowNull: true,
    },
    Password: {
      type: STRING(20),
      allowNull: true,
    },
    ClassesID: {
      type: INTEGER,
      allowNull: true,
    },
    Remark: {
      type: STRING(300),
      allowNull: true,
    },
    Photo: {
      type: BLOB,
      allowNull: true,
    },
    ServiceID: {
      type: INTEGER,
      allowNull: true,
    },
    UseFaceRecognition: {
      type: INTEGER,
      allowNull: true,
    },
    FaceID: {
      type: STRING(50),
      allowNull: true,
    },
    MachineID: {
      type: INTEGER,
      allowNull: true,
    },
    FeatureType: {
      type: INTEGER,
      allowNull: true,
    },
    MachinePrivilege: {
      type: INTEGER,
      allowNull: true,
    },
    PasswordData: {
      type: BLOB,
      allowNull: true,
    },
    CardNoData: {
      type: BLOB,
      allowNull: true,
    },
    FingerData: {
      type: BLOB,
      allowNull: true,
    },
    FaceData: {
      type: BLOB,
      allowNull: true,
    },
    PCode: {
      type: STRING(20),
      allowNull: true,
    },
    ExData: {
      type: BLOB,
      allowNull: true,
    },
  }, {
    freezeTableName: true,
    tableName: 'personnel',
    timestamps: true,
    createdAt: 'CreateDateTime',
    updatedAt: 'ModifyDateTime',
  });
  Personnel.associate = function() {
    app.accessControlModel.Personnel.belongsTo(app.accessControlModel.DepartmentInfo, { as: 'objectinfo', foreignKey: 'ParentID', targetKey: 'ID' });
  };
  return Personnel;
};
