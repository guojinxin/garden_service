/*
 * @Author: guojinxin_hub 1907745233@qq.com
 * @Date: 2023-06-06 15:29:56
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-06-06 19:37:18
 * @FilePath: \garden_service\app\accessControl_model\objectinfo.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';

/**
 * BbJectInfo 部门表
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { STRING, INTEGER } = app.Sequelize;
  const DepartmentInfo = app.accessControlModel.define(
    'objectinfo',
    {
      ID: {
        type: INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      TypeID: {
        type: INTEGER,
        allowNull: false,
      },
      Name: {
        type: STRING(50),
        allowNull: false,
      },
    },
    {
      freezeTableName: true,
      tableName: 'objectinfo',
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );
  return DepartmentInfo;
};
