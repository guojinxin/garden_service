/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-29 09:37:38
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-07-31 16:08:50
 * @FilePath: \garden_service\app\schedule\addVisit.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
const Subscription = require('egg').Subscription;
const moment = require('moment');
const uuid = require('uuid');
const Op = require('sequelize').Op;
const _ = require('lodash');
class AddVisit extends Subscription {
  // 通过 schedule 属性来设置定时任务的执行间隔等配置
  static get schedule() {
    return {
      cron: '59 59 23 * * *', //  60s间隔  支持interval和corn的定时方式
      type: 'worker',
      // env: [ 'prod' ],
      disable: false,
      immediate: false,
    };
  }

  // subscribe 是真正定时任务执行时被运行的函数
  /**
   * 访客连续访问 每天23点59分59秒生成访客
   */
  async subscribe() {
    const { ctx } = this;
    const options = {
      where: { visitEndTime: { [Op.gte]: moment(new Date()).valueOf() }, status: { [Op.in]: [ '1', '2', '4', '5' ] }, isContinuousVisit: true },
      order: [[ 'created_time', 'DESC' ]],
      raw: true,
    };
    const visitUserList = await ctx.model.YVisitUser.findAll(options);
    const visitUserIdList = [];
    await visitUserList.forEach(item => {
      if (!_.isEmpty(item)) {
        item.id = uuid.v1();
        if (moment(item.visitEndTime).format('YYYY-MM-DD') !== moment(new Date()).format('YYYY-MM-DD')) {
          item.visitStrTime = moment(new Date()).add(2, 'minute').format('YYYY-MM-DD') + ' 00:00:00';
          item.visitEndTime = moment(new Date()).add(23.58, 'hour').format('YYYY-MM-DD HH:mm:ss');
        }
        item.isContinuousVisit = false;
        item.status = '1';
        item.created_time = moment(new Date()).add(2, 'minute').format('YYYY-MM-DD') + ' 00:00:00';
        item.updated_time = moment(new Date()).add(2, 'minute').format('YYYY-MM-DD') + ' 00:00:00';
        visitUserIdList.push(item);
      }
    });
    await ctx.model.YVisitUser.bulkCreate(visitUserIdList);
  }
}

module.exports = AddVisit;
