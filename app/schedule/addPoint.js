/*
 * @Author: guojinxin_hub 1907745233@qq.com
 * @Date: 2023-04-10 19:47:51
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-06-15 10:31:38
 * @FilePath: \garden_service\app\schedule\addPatrol.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-29 09:37:38
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-06-06 20:06:57
 * @FilePath: \garden_service\app\schedule\addVisit.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
const Subscription = require('egg').Subscription;
const _ = require('lodash');

class AddPatrol extends Subscription {
  // 通过 schedule 属性来设置定时任务的执行间隔等配置
  static get schedule() {
    return {
      interval: '60000s', //  60s间隔  支持interval和corn的定时方式
      type: 'worker',
      // env: [ 'prod' ],
      disable: false,
      immediate: true,
    };
  }

  // subscribe 是真正定时任务执行时被运行的函数
  /**
   * 访客连续访问 每天23点59分59秒生成访客
   */
  async subscribe() {
    const { ctx } = this;
    const token = await ctx.curl(
      'https://api.5ixun.com/api/v1/login?username=15964369623&password=e94fd8eb04ce6213b9341fb142ec9a2e',
      { dataType: 'json' }
    );
    if (!_.isEmpty(token.data.data)) {
      this.ctx.session.patrolToken = token.data.data;
    }
    const checkPointData = await ctx.curl(
      'https://api.5ixun.com/api/v1/checkpoint',
      {
        method: 'GET',
        data: {
          pageNum: 1,
          pageSize: 200,
        },
        headers: {
          Authorization: _.isEmpty(token.data.data) ? this.ctx.session.patrolToken : token.data.data,
        },
        contentType: 'json',
        dataType: 'json',
      }
    );
    const pointData = await ctx.model.YPoint.findAll({ raw: true });
    if (!_.isEmpty(checkPointData)) {
      pointData.forEach(async item => {
        await ctx.model.YPoint.destroy({
          where: { id: item.id },
        }); // 先删除

      });
    }
    if (checkPointData.data?.data?.rows) {
      await ctx.model.YPoint.bulkCreate(checkPointData.data.data.rows);
    }
  }
}

module.exports = AddPatrol;
