/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-29 09:37:38
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-06-15 10:26:57
 * @FilePath: \garden_service\app\schedule\addVisit.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
const Subscription = require('egg').Subscription;
const moment = require('moment');
const _ = require('lodash');
const Op = require('sequelize').Op;

class AddPatrol extends Subscription {
  // 通过 schedule 属性来设置定时任务的执行间隔等配置
  static get schedule() {
    return {
      interval: '600s', //  60s间隔  支持interval和corn的定时方式
      type: 'worker',
      // env: [ 'prod' ],
      disable: false,
      immediate: true,
    };
  }

  // subscribe 是真正定时任务执行时被运行的函数
  /**
   * 访客连续访问 每天23点59分59秒生成访客
   */
  async subscribe() {
    const { ctx } = this;
    const token = await ctx.curl(
      'https://api.5ixun.com/api/v1/login?username=15964369623&password=e94fd8eb04ce6213b9341fb142ec9a2e',
      { dataType: 'json' }
    );
    if (!_.isEmpty(token.data.data)) {
      this.ctx.session.patrolToken = token.data.data;
    }
    const logData = await ctx.curl(
      'https://api.5ixun.com/api/v1/checkpointLog',
      {
        method: 'GET',
        data: {
          startTime: Date.parse(moment().format('YYYY-MM-DD') + ' 00:00:00'),
          endTime: Date.parse(moment().format('YYYY-MM-DD') + ' 23:59:59'),
          pageNum: 1,
          pageSize: 200,
        },

        headers: {
          Authorization: _.isEmpty(token.data.data) ? this.ctx.session.patrolToken : token.data.data,
          // Authorization: _.isEmpty(token.data.data) ? 'eyJhbGciOiJIUzUxMiIsInppcCI6IkRFRiJ9.eNosyksKgzAQANC7zNqBjMl89DZJiGALWkxaCuLdTaHbxzvh0VaYIecxJtEFczbB4JcJU4gjek5KRo6dUxigvlPPxCQTCatZt7XWbm1_lg1rOT7l-GFsMJMYkQsqOkD5vv7gPYteNwAAAP__.1_A6MqnnRduHFSuvD0z8ZuBCpsB2XndPLqMxj6uPGelZddkAgrvR-n8jLJZ3jHdpBEX7j0U4WhCYC8djUZ4Bsg' : token.data.data,
        },
        contentType: 'json',
        dataType: 'json',
      }
    );
    const patrolData = await ctx.model.YPatrol.findAll({ where: { createTime: {
      [Op.lte]: moment().format('YYYY-MM-DD') + ' 23:59:59',
      [Op.gte]: moment().format('YYYY-MM-DD') + ' 00:00:00',
    } }, raw: true });

    if (!_.isEmpty(patrolData)) {
      patrolData.forEach(async item => {
        await ctx.model.YPatrol.destroy({
          where: { checkpointId: item.checkpointId },
        }); // 先删除
      });
    }
    if (logData.data?.data?.rows) {
      await ctx.model.YPatrol.bulkCreate(logData.data.data.rows);
    }
  }
}

module.exports = AddPatrol;
