/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-29 09:37:38
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-04-28 13:21:52
 * @FilePath: \garden_service\app\schedule\addVisit.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
const Subscription = require('egg').Subscription;
const moment = require('moment');
const _ = require('lodash');
const Op = require('sequelize').Op;
class AddUseTwoWater extends Subscription {
  // 通过 schedule 属性来设置定时任务的执行间隔等配置
  static get schedule() {
    return {
      interval: '8000s', //  60s间隔  支持interval和corn的定时方式
      type: 'worker',
      // env: [ 'prod' ],
      disable: false,
      immediate: true,
    };
  }

  // subscribe 是真正定时任务执行时被运行的函数
  /**
     * 访客连续访问 每天23点59分59秒生成访客
     */
  async subscribe() {
    const { ctx } = this;
    const res = await this.ctx.curl('http://120.25.227.96:24201/iotserver/v1/lastlog', {
      method: 'POST',
      headers: {
        appKey: '9b58bf9421a241c23e889d83e43ca4b2',
      },
      data: {
        meterNo: '2206200400',
      },
      contentType: 'json',
      dataType: 'json',
    }
    );
    const body = {
      name: res.data.data[0].meterNo,
      machineData: res.data.data[0].meterJbs,
      position: '原液车间北侧制水间',
      created_time: res.data.data[0].readMeterTime,
      isAutomatic: true,
      entry_time: moment(res.data.data[0].readMeterTime).format('YYYY-MM'),
    };
    const options = {
      where: { name: body.name, entry_time: { [Op.lt]: moment(body.entry_time).format('YYYY-MM') }, isDelete: false },
      order: [[ 'entry_time', 'DESC' ]],
      limit: 1,
    };
      // 上个月的数据
    const oldUseWater = await ctx.model.YUseWater.findAll(options);
    if (!_.isEmpty(oldUseWater)) {
      body.monthlyDosage = body.machineData - oldUseWater[0].machineData;
    } else {
      body.monthlyDosage = body.machineData;
    }
    let transaction;
    try {
      transaction = await this.ctx.model.transaction();
      const useWaterOptions = {
        where: { entry_time: moment(body.entry_time).format('YYYY-MM'), name: body.name, isDelete: false },
        order: [[ 'entry_time', 'DESC' ]],
        limit: 1,
      };
      const useWaterData = await ctx.model.YUseWater.findOne(useWaterOptions);
      if (!_.isEmpty(useWaterData)) {
        await useWaterData.destroy();
      }
      // // 根据统计日期查询
      // const totalEnergyData = await ctx.model.YTotalEnergy.findAll({ where: { created_time: moment(body.entry_time).format('YYYY-MM'), type: '0' } });
      // if (!_.isEmpty(totalEnergyData)) {
      //   totalEnergyData.forEach(async item => {
      //     await ctx.model.YTotalEnergy.destroy({ where: { id: item.id } }); // 先删除
      //   });
      // }
      // // 查询当前统计日期所有电表的当月用电量
      // const allOptions = {
      //   where: { entry_time: moment(body.entry_time).format('YYYY-MM'), isDelete: false },
      //   attributes: [ 'monthlyDosage' ],
      // };
      // const allUseWater = await ctx.model.YUseWater.findAll(allOptions);
      // let allMonthlyDosage = body.monthlyDosage;
      // allUseWater.forEach(item => {
      //   allMonthlyDosage = Number(allMonthlyDosage) + Number(item.monthlyDosage);
      // });
      // const totalEnergyBody = {
      //   total: allMonthlyDosage,
      //   created_time: moment(body.entry_time).format('YYYY-MM'),
      //   type: '0',
      // };
      // await ctx.model.YTotalEnergy.create(totalEnergyBody);
      await ctx.model.YUseWater.create(body);

      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      console.log(error);
    }
  }
}

module.exports = AddUseTwoWater;
