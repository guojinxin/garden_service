/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-26 10:57:52
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-04-10 20:39:15
 * @FilePath: \garden_service\app\extend\helper.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const uuid = require('uuid');

module.exports = {
  /**
   * 生成 uuid
   * @return {String} uuid
   */

  timestampToTime(timestamp) {
    // 时间戳为10位需*1000，时间戳为13位不需乘1000
    const date = new Date(timestamp * 1000 / 1000);
    const Y = date.getFullYear() + '-';
    const M =
    (date.getMonth() + 1 < 10
      ? '0' + (date.getMonth() + 1)
      : date.getMonth() + 1) + '-';
    const D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
    const h = date.getHours() + ':';
    const m = date.getMinutes() + ':';
    const s = date.getSeconds();
    return Y + M + D + h + m + s;
  },
  uuid() {
    return uuid.v1();
  },
};
