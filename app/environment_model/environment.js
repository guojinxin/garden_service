/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2023-01-16 15:32:49
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-01-17 15:59:33
 * @FilePath: \garden_service\app\environment_model\environment.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

'use strict';

/**
 * qixiangzhan
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { INTEGER, DOUBLE, STRING } = app.Sequelize;
  const Environment = app.environmentModel.define('qixiangzhan', {
    id: {
      type: INTEGER(8),
      primaryKey: true,
      autoIncrement: true,
    },
    leixing: {
      type: STRING,
    },
    shuzhi: {
      type: DOUBLE,
    },
  }, {
    freezeTableName: true,
    tableName: 'qixiangzhan',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
  });
  return Environment;
};
