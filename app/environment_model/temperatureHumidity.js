/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2023-01-16 15:44:53
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-01-16 15:51:05
 * @FilePath: \garden_service\app\environment_model\wenshidu.JS
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

'use strict';

/**
 * qixiangzhan
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { INTEGER, DOUBLE, STRING } = app.Sequelize;
  const TemperatureHumidity = app.environmentModel.define('wenshidu', {
    id: {
      type: INTEGER(8),
      primaryKey: true,
      autoIncrement: true,
    },
    weizhi: {
      type: STRING,
    },
    wendu: {
      type: DOUBLE,
    },
    shidu: {
      type: DOUBLE,
    },
  }, {
    freezeTableName: true,
    tableName: 'wenshidu',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
  });
  return TemperatureHumidity;
};
