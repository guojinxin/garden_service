/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-11-09 13:20:37
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-06-06 11:13:36
 * @FilePath: \garden_service\app\parked_model\parked.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const moment = require('moment');

/**
 * Parked
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { INTEGER, DATE, DECIMAL, STRING } = app.Sequelize;
  const Parked = app.mssql.define('parked', {
    parked_id: {
      type: INTEGER(4),
      primaryKey: true,
      autoIncrement: true,
    },
    card_id: {
      type: STRING,
      allowNull: true,
    },
    in_sbname: {
      type: STRING,
      allowNull: true,
    },
    out_sbname: {
      type: STRING,
      allowNull: true,
    },
    car_cp: {
      type: STRING(45),
      allowNull: true,
      get() {
        return this.getDataValue('car_cp');
      },
    },
    in_time: {
      type: DATE(8),
      allowNull: false,
      get() {
        return moment(this.getDataValue('in_time')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    out_time: {
      type: DATE(8),
      allowNull: false,
      get() {
        return moment(this.getDataValue('out_time')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    card_kind: {
      type: STRING(20),
      allowNull: true,
    },
    Operator_name: {
      type: STRING(20),
      allowNull: true,
    },
    parked_fee: {
      type: INTEGER(4),
      allowNull: true,
    },
    parked_zkfee: {
      type: INTEGER(4),
      allowNull: true,
    },
    park_id: {
      type: INTEGER(4),
      allowNull: true,
    },
    fee_lb: {
      type: STRING(20),
      allowNull: true,
    },
    card_selfno: {
      type: STRING(45),
      allowNull: true,
    },
    user_name: {
      type: STRING(45),
      allowNull: true,
    },
    park_zk: {
      type: STRING(45),
      allowNull: true,
    },
    inbeizhu: {
      type: STRING(60),
      allowNull: true,
    },
    outbeizhu: {
      type: STRING(300),
      allowNull: true,
    },
    IsYBPay: {
      type: INTEGER(2),
      allowNull: true,
    },
    Sendflag: {
      type: INTEGER(4),
      allowNull: true,
    },
    LBPayFlag: {
      type: INTEGER(4),
      allowNull: true,
    },
    YBPayNo: {
      type: STRING(50),
      allowNull: true,
    },
    WeChatPayMoney: {
      type: INTEGER(4),
      allowNull: true,
    },
    WeChatPayTime: {
      type: DATE,
      allowNull: true,
    },
    ZJBD_TID: {
      type: STRING(50),
      allowNull: true,
    },
    InternetOrder: {
      type: INTEGER(4),
      allowNull: true,
    },
    AutoPayCheck: {
      type: INTEGER(4),
      allowNull: true,
    },
    AccountBalance: {
      type: DECIMAL(9),
      allowNull: true,
    },
    AutoPayMoney: {
      type: INTEGER(4),
      allowNull: true,
    },
    CentreReceiveMoney: {
      type: INTEGER(4),
      allowNull: true,
    },
    CentrePayMoney: {
      type: INTEGER(4),
      allowNull: true,
    },
    CentrePayTime: {
      type: DATE,
      allowNull: true,
    },
    CentrePayOperatorName: {
      type: STRING(50),
      allowNull: true,
    },
    WeChatPayMoneyReceive: {
      type: INTEGER(4),
      allowNull: true,
    },
    ToMonthCardTime: {
      type: DATE,
      allowNull: true,
    },
    PlateColor: {
      type: STRING(20),
      allowNull: true,
    },
    CalcFeeTime: {
      type: DATE,
      allowNull: true,
    },
    centerFeeLB: {
      type: STRING(50),
      allowNull: true,
    },
    RedPacketUrl: {
      type: STRING(500),
      allowNull: true,
    },
    isNoInPark: {
      type: INTEGER(1),
      allowNull: true,
    },
    pmoney: {
      type: INTEGER(4),
      allowNull: true,
    },
    zkmoney: {
      type: INTEGER(4),
      allowNull: true,
    },
    AFee: {
      type: INTEGER(4),
      allowNull: true,
    },
    InOperator_name: {
      type: STRING(20),
      allowNull: true,
    },
    outRoadID: {
      type: INTEGER(4),
      allowNull: true,
    },
    start_charging_time: {
      type: Date,
      allowNull: true,
    },
    stop_charging_time: {
      type: Date,
      allowNull: true,
    },
    checked: {
      type: INTEGER(4),
      allowNull: true,
    },
    quryPriceTime: {
      type: Date,
      allowNull: true,
    },
    SendFlag1: {
      type: INTEGER(4),
      allowNull: true,
    },
  }, {
    freezeTableName: true,
    tableName: 'parked',
    timestamps: true,
    createdAt: 'in_time',
    updatedAt: 'out_time',
  });
  Parked.associate = function() {
    app.mssql.Parked.belongsTo(app.mssql.Mfeecard, { as: 'mFeeCard', foreignKey: 'card_id', targetKey: 'card_id' });
  };
  return Parked;
};
