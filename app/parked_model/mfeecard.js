/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-11-09 13:20:37
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-06-06 14:31:50
 * @FilePath: \garden_service\app\parked_model\mfeecard.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';

/**
 * Mfeecard
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { INTEGER, STRING } = app.Sequelize;
  const Mfeecard = app.mssql.define('car', {
    card_id: {
      type: STRING,
      primaryKey: true,
      autoIncrement: true,
    },
    card_kind: {
      type: STRING,
      allowNull: true,
    },
    card_zt: {
      type: STRING,
      allowNull: true,
    },
    card_bz: {
      type: STRING,
      allowNull: true,
    },
    card_selfno: {
      type: STRING,
      allowNull: true,
    },
    card_creatof: {
      type: STRING,
      allowNull: true,
    },
    user_bh: {
      type: STRING,
      allowNull: true,
    },
    check1: {
      type: INTEGER,
      allowNull: true,
    },
    ParkID: {
      type: INTEGER,
      allowNull: true,
    },
  }, {
    freezeTableName: true,
    tableName: 'car',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
  });
  return Mfeecard;
};
