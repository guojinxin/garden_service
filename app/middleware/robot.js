/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-05 16:24:44
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-08-16 14:00:08
 * @FilePath: \garden_service\app\middleware\robot.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// app/middleware/robot.js
// options === app.config.robot
module.exports = options => {
  return async function robotMiddleware(ctx, next) {
    const source = ctx.get('user-agent') || '';
    const match = options.ua.some(ua => ua.test(source));
    if (match) {
      ctx.status = 403;
      ctx.message = 'Go away, robot.';
    } else {
      await next();
    }
  };
};

// config/config.default.js
// add middleware robot
exports.middleware = [ 'robot' ];
// robot's configurations
exports.robot = {
  ua: [ /Baiduspider/i ],
};
