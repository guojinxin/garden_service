/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-17 14:17:47
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-06-05 16:56:32
 * @FilePath: \garden_service\app\middleware\jwt.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// middleware/jwt.js
// 在 “router 中使用中间件” 中用不到
const whiteList = [ '/login' ];
const _ = require('lodash');
module.exports = () => {
  return async function(ctx, next) {
    // 判断接口路径是否在白名单（在 “router 中使用中间件”中不需要验证这一步）
    const isInWhiteList = whiteList.some(item => item === ctx.request.url);
    if (!isInWhiteList) {
      // 拿到前端传过来的 token
      const token = ctx.request.header.authorization;
      if (token) {
        // 解密token
        const secret = ctx.app.config.jwt.secret;
        const decoded = ctx.app.jwt.verify(token, secret) || 'false';
        const app_user = await ctx.app.sessionStore.get(token);
        if (decoded !== 'false' && !_.isEmpty(app_user)) {
          ctx.session.app_user = app_user;
          await next();
        } else {
          ctx.throw(403, '无效Token');
        }
      } else {
        ctx.throw(403, '无Token');
      }
    } else {
      await next();
    }
  };
};

