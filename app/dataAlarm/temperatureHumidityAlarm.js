/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2023-02-07 15:38:05
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-02-07 16:02:16
 * @FilePath: \garden_service\app\data\temperatureHumidityAlarm.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
const data = [
  'ZHL-1-大厅-温湿度变送记录仪',
  'ZHL-1-服务器机房-温湿度变送记录仪',
  'ZHL-2-敞开办公室-温湿度变送记录仪',
  'ZHL-2-办公室-温湿度变送记录仪',
  'ZHL-2-茶水间-温湿度变送记录仪',
];
module.exports = data;
