/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-17 13:06:42
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-03-03 10:12:10
 * @FilePath: \garden_service\app\service\user.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';

const Service = require('egg').Service;
const crypto = require('crypto');

function toInt(str) {
  if (typeof str === 'number') return str;
  if (!str) return str;
  return parseInt(str, 10) || 0;
}
class UserService extends Service {
  // 查询user表，验证密码和花名
  async validUser(nickname, password) {
    const data = await this.getUser();
    const pwd = crypto.createHash('md5').update(password).digest('hex');
    for (const item of data) {
      if (item.nickname === nickname && item.password === pwd) {
        this.app.sessionStore.set('user', item);
        this.ctx.session.user = item;
        return item;
      }
    }
    return false;
  }

  // 获取用户，不传id则查询所有
  async getUser(id) {
    const { ctx } = this;
    const query = { limit: toInt(ctx.query.limit), offset: toInt(ctx.query.offset) };
    if (id) {
      return await ctx.model.YAdminUser.findByPk(toInt(id));
    }
    return await ctx.model.YAdminUser.findAll(query);
  }
  // 专门对数据进行md5加密的方法，输入明文返回密文
  getMd5Data(data) {
    return crypto.createHash('md5').update(data).digest('hex');
  }
}
module.exports = UserService;
