/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-15 09:50:33
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-04-03 16:43:05
 * @FilePath: \garden_service\app\service\energy.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const moment = require('moment');
const Service = require('egg').Service;
const _ = require('lodash');
class EnergyService extends Service {
  async getWaterLastYearData(date, type) {
    const data = await this.ctx.model.YTotalEnergy.findOne({ where: { created_time: moment(date).format('YYYY-MM'), type }, attributes: [ 'total' ] });
    if (_.isEmpty(data)) {
      return 0;
    }
    return data.total.toFixed(2);
  }
  async getWaterTotalLastYearData(date, type) {
    const data = await this.ctx.model.YTotalEnergy.findOne({ where: { created_time: moment(date).format('YYYY-MM'), type }, attributes: [ 'total' ] });
    if (_.isEmpty(data)) {
      return 0;
    }
    if (type === '1') {
      return (data.total / 1000).toFixed(2);
    }
    return (data.total / 100).toFixed(2);
  }

  async getAmountYearData(date, type) {
    const data = await this.ctx.model.YTotalEnergy.findOne({ where: { created_time: moment(date).format('YYYY-MM'), type }, attributes: [ 'amount_total' ] });
    if (_.isEmpty(data)) {
      return 0;
    }
    return data.amount_total ? (data.amount_total / 10000).toFixed(2) : 0;
  }
}
module.exports = EnergyService;
