/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-15 09:50:33
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-04-19 18:24:10
 * @FilePath: \garden_service\app\service\energy.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const moment = require('moment');
const Service = require('egg').Service;
const nodemailer = require('nodemailer');
const _ = require('lodash');
const Op = require('sequelize').Op;

class SendAlarmService extends Service {
  async getAlarm(date) {
    const data = await this.ctx.model.YAlarm.findAndCountAll({
      where: { alarm_time: { [Op.gte]: moment(date).format('YYYY-MM'), [Op.lt]: moment(date).add(1, 'M').format('YYYY-MM') } },
      attributes: [ 'alarm_time' ],
    });
    if (_.isEmpty(data)) {
      return 0;
    }
    return data.count ? data.count : 0;
  }
  async sendAlarm(params) {
    const { ctx, config } = this;
    await ctx.curl(`${config.uinoPath + '/thing/dix/monitor/alarm'}`, {
      method: 'POST',
      data: [
        {
          cikey: params.cikey,
          severity: params.severity,
          status: 1,
          kpiname: 'gatepost',
          summary: params.summary,
          others: '',
          lasttime: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        },
      ],
      contentType: 'json',
      dataType: 'json',
    });
    const transporter = nodemailer.createTransport({
      service: 'qq',
      auth: {
        user: 'adsw-xxfs@qq.com', // generated ethereal user
        pass: 'rtbbnyejufamdggd', // generated ethereal password
      },
    });
    const userList = await ctx.model.YAdminUser.findAll({
      where: {
        [Op.and]: [
          { isDelete: false },
          ctx.model.fn(
            'JSON_CONTAINS',
            ctx.model.col('alarmPerson'),
            JSON.stringify(params.alarmPerson)
          ),
        ],
      },
    });
    const emailList = await _.map(userList, 'email');

    // send mail with defined transport object
    await transporter.sendMail({
      from: 'adsw-xxfs@qq.com', // sender address
      to: emailList.toString() || 'adsw-xxfs@qq.com', // list of receivers
      subject: params.subject, // Subject line
      text: params.text, // plain text body
      html: `<b>${params.summary || ''}</b>`, // html body
    });
    const gatepostData = [];

    if (_.isEmpty(userList)) {
      gatepostData.push({
        alarm_time: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        modify_time: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        alarm_information: params.summary,
        alarmlevel: params.severity,
        status: 1,
        ciName: params.cikey,
        alarmType: params.alarmPerson,
        distributionUser_Id: '89d96250-28ff-11ed-b1ee-e971ecbde9bf',
      });
    } else {
      userList.forEach(item => {
        gatepostData.push({
          alarm_time: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
          modify_time: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
          alarm_information: params.summary,
          alarmlevel: params.severity,
          status: 1,
          ciName: params.cikey,
          alarmType: params.alarmPerson,
          distributionUser_Id: item.id,
        });
      });
    }
    await ctx.model.YAlarm.bulkCreate(gatepostData);
  }
  async sendAlarmList(params) {
    // const queue = []; // 队列数组
    // function enqueue(item, priority = 0) {
    //   // 向队列中添加一个待处理项，priority 表示处理的优先级
    //   queue.push({ item, priority });
    // }
    // function dequeue() {
    //   // 从队列中获取一个待处理项，并将其从队列中删除
    //   if (queue.length === 0) {
    //     return null;
    //   }
    //   let maxPriority = -Infinity;
    //   let maxPriorityIndex = -1;
    //   for (let i = 0; i < queue.length; i++) {
    //     if (queue[i].priority > maxPriority) {
    //       maxPriority = queue[i].priority;
    //       maxPriorityIndex = i;
    //     }
    //   }
    //   return queue.splice(maxPriorityIndex, 1)[0].item;
    // }
    // function processQueue() {
    //   // 从队列中获取一个待处理项，并对其进行处理
    //   const item = dequeue();
    //   if (item) {
    //     // 处理完当前项后，继续处理队列中的下一项
    //     setTimeout(processQueue, 1088);
    //   }

    // }
    // // 向队列中添加一些待处理项
    // enqueue('item 1', 1);
    // enqueue('item 2', 2);
    // enqueue('item 3', 3);
    // // 开始处理队列中的待处理项
    // processQueue();
    const { ctx, config } = this;
    await ctx.curl(`${config.uinoPath + '/thing/dix/monitor/alarm'}`, {
      method: 'POST',
      data: [
        {
          cikey: params.cikey,
          severity: params.severity,
          status: 1,
          kpiname: 'gatepost',
          summary: params.summary,
          others: '',
          lasttime: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        },
      ],
      contentType: 'json',
      dataType: 'json',
    });
    const transporter = nodemailer.createTransport({
      service: 'qq',
      auth: {
        user: 'adsw-xxfs@qq.com', // generated ethereal user
        pass: 'rtbbnyejufamdggd', // generated ethereal password
      },
    });
    const userList = await ctx.model.YAdminUser.findAll({
      where: {
        [Op.and]: [
          { isDelete: false },
          ctx.model.fn(
            'JSON_CONTAINS',
            ctx.model.col('alarmPerson'),
            JSON.stringify(params.alarmPerson)
          ),
        ],
      },
    });
    const emailList = await _.map(userList, 'email');

    // send mail with defined transport object
    await transporter.sendMail({
      from: 'adsw-xxfs@qq.com', // sender address
      to: emailList.toString() || 'adsw-xxfs@qq.com', // list of receivers
      subject: params.subject, // Subject line
      text: params.text, // plain text body
      html: `<b>${params.summary || ''}</b>`, // html body
    });
    const gatepostData = [];

    if (_.isEmpty(userList)) {
      gatepostData.push({
        alarm_time: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        modify_time: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        alarm_information: params.summary,
        alarmlevel: params.severity,
        status: 1,
        ciName: params.cikey,
        alarmType: params.alarmPerson,
        distributionUser_Id: '89d96250-28ff-11ed-b1ee-e971ecbde9bf',
      });
    } else {
      userList.forEach(item => {
        gatepostData.push({
          alarm_time: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
          modify_time: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
          alarm_information: params.summary,
          alarmlevel: params.severity,
          status: 1,
          ciName: params.cikey,
          alarmType: params.alarmPerson,
          distributionUser_Id: item.id,
        });
      });
    }
    await ctx.model.YAlarm.bulkCreate(gatepostData);
  }
}
module.exports = SendAlarmService;
