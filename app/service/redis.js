/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-09-25 19:18:39
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-12-01 10:31:42
 * @FilePath: \garden_service\app\service\redis.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

const Service = require('egg').Service;
const time = 60 * 60 * 24 * 365; // 默认缓存失效时间 365天
class RedisService extends Service {
  // 设置
  async set(key, value, seconds) {
    // seconds 有效时长
    const { redis } = this.app;
    value = JSON.stringify(value);
    if (!seconds) {
      // await redis.set(key, value);
      await redis.set(key, value, 'EX', time);
    } else {
      // 设置有效时间
      await redis.set(key, value, 'EX', seconds);
    }
  }
  // 获取
  async get(key) {
    const { redis } = this.app;
    let data = await redis.get(key);
    if (!data) return;
    data = JSON.parse(data);
    return data;
  }
  // 清空redis
  async flushall() {
    const { redis } = this.app;
    redis.flushall();
    return;
  }
}
module.exports = RedisService;
