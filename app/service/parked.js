/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-15 09:50:33
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-09-08 14:54:57
 * @FilePath: \garden_service\app\service\energy.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const moment = require('moment');
const Service = require('egg').Service;
const _ = require('lodash');
const Op = require('sequelize').Op;
const sequelize = require('sequelize');

class EnergyService extends Service {
  async getParkedStatistics(start_time, end_time) {
    const option = {
      where: {},
      attributes: [ 'parked_id' ],
    };
    if (!_.isEmpty(end_time)) {
      option.where = { out_time: { [Op.gte]: start_time, [Op.lte]: end_time } };
    } else {
      option.where = { out_time: { [Op.gte]: moment().format('YYYY-MM-DD') } };
    }
    const data = await this.ctx.mssql.Parked.findAndCountAll(option);
    return data.count || '0';
  }
  async getVisitUserCount() {
    const { ctx } = this;
    const options = {
      where: { statusName: { [Op.in]: [ '已到访' ] }, created_time: {
        [sequelize.Op.lt]: moment().format('YYYY-MM-DD') + ' 23:59:59',
        [sequelize.Op.gt]: moment().format('YYYY-MM-DD') + ' 00:00:00',
      } },
      attributes: [ 'visit_Id' ],
      order: [[ 'created_time', 'DESC' ]],
    };
    const visitStateData = await ctx.model.YVisitState.findAll(options);
    const visitIdList = _.map(visitStateData, 'visit_Id');
    const visitOptions = {
      where: { visitId: { [Op.in]: visitIdList } },
      attributes: [ 'visitUnit', 'name', 'licensePlateNum', 'updated_time', 'address', 'status', 'visitId' ],
      order: [[ 'updated_time', 'DESC' ]],
    };
    const visitUserData = await ctx.model.YVisitUser.findAll(visitOptions);
    const arr1 = _.uniqBy(visitUserData, 'visitId');
    return arr1.length;
  }
  async getParkedCount() {
    const { ctx } = this;
    const options = {
      where: { in_time: {
        [sequelize.Op.lt]: moment().format('YYYY-MM-DD') + ' 23:59:59',
        [sequelize.Op.gt]: moment().format('YYYY-MM-DD') + ' 00:00:00',
      },
      },
      attributes: [ 'car_cp', 'in_time', 'out_time' ],
      order: [[ 'in_time', 'DESC' ]],
    };
    const data = await ctx.mssql.Parked.findAll(options);
    return data.length;
  }
  async getOutParkedCount() {
    const { ctx } = this;
    const options = {
      where: { in_time: {
        [sequelize.Op.lt]: moment().format('YYYY-MM-DD') + ' 23:59:59',
        [sequelize.Op.gt]: moment().format('YYYY-MM-DD') + ' 00:00:00',
      }, out_time: { [sequelize.Op.ne]: null } },
      attributes: [ 'car_cp', 'in_time', 'out_time' ],
      order: [[ 'out_time', 'DESC' ]],
    };
    const data = await ctx.mssql.Parked.findAll(options);
    return data.length;
  }
  // 门禁刷卡
  async getSwingCardCount() {
    const { ctx } = this;
    const options = {
      where: { CreateDateTime: { [Op.gte]: moment().format('YYYY-MM-DD'), [Op.lte]: moment(new Date()).format('YYYY-MM-DD HH:mm:ss') } },
      include: [{ model: ctx.accessControlModel.Personnel, as: 'personnel', attributes: [ 'ID', 'Name' ], required: false }],
      attributes: [ 'ID', 'PersonnelID', 'CreateDateTime', 'DevLocation' ],
      order: [[ 'CreateDateTime', 'DESC' ]],
    };
    const swingCardData = await ctx.accessControlModel.SwingCard.findAll(options);
    return swingCardData.length;
  }
}
module.exports = EnergyService;
