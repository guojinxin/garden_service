/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-15 09:50:33
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-01-31 10:55:29
 * @FilePath: \garden_service\app\service\energy.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict';
const moment = require('moment');
const Service = require('egg').Service;
const _ = require('lodash');
const Op = require('sequelize').Op;
const sequelize = require('sequelize');

sequelize.DATE.prototype._stringify = function _stringify(date, options) {
  return this._applyTimezone(date, options).format('YYYY-MM-DD HH:mm:ss.SSS');
};
class EnergyService extends Service {
  async getAttendanceCount(start_time, end_time) {
    const option = {
      where: {},
      group: [ 'EmployeeID', 'CardTime' ],
      attributes: [ 'EmployeeID', [ this.ctx.attendanceMssql.Sequelize.fn('COUNT', 'EmployeeID'), 'employee_count' ], 'CardTime' ],
      order: [[ this.ctx.attendanceMssql.Sequelize.fn('COUNT', 'EmployeeID'), 'DESC' ]],
      raw: true,
    };
    if (!_.isEmpty(end_time)) {
      option.where = { CardTime: { [Op.gte]: start_time, [Op.lte]: end_time } };
    } else {
      option.where = { CardTime: { [Op.gte]: moment().format('YYYY-MM-DD') } };
    }
    const data = await this.ctx.attendanceMssql.Card.findAll(option);
    return data.length || '0';
  }
}
module.exports = EnergyService;
