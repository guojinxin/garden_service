/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-08 14:03:45
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-09-14 13:53:32
 * @FilePath: \garden_service\database\migrations\20220808060345-init-y_admin_user.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

'use strict';

module.exports = {
  // 在执行数据库升级时调用的函数，创建 users 表
  up: async (queryInterface, Sequelize) => {
    const { DATE, STRING, ENUM, BOOLEAN } = Sequelize;
    await queryInterface.createTable('y_admin_user', {
      id: { type: STRING(36), primaryKey: true },
      nickname: STRING(20),
      password: STRING(50),
      name: STRING(50),
      gender: ENUM('0', '1'),
      phone: STRING(50),
      jobNum: STRING(50),
      department: STRING(50),
      role: ENUM('0', '1', '2', '3'),
      isApproval: BOOLEAN,
      created_time: DATE,
    });
    await queryInterface.createTable('y_visit_user', {
      id: { type: STRING(36), primaryKey: true },
      visitId: { type: STRING(36), primaryKey: true },
      isContinuousVisit: BOOLEAN,
      name: STRING(50),
      idNum: STRING(50),
      visitTime: DATE,
      visitStrTime: DATE,
      visitEndTime: DATE,
      status: ENUM('0', '1', '2', '3', '4', '5', '6'),
      address: STRING(50),
      visitUnit: STRING(50),
      licensePlateNum: STRING(200),
      healthCode: STRING(200),
      tourCode: STRING(200),
      describe: STRING(300),
      createUser_Id: STRING(300),
      approver_Id: STRING(300),
      estimatedStrTime: DATE,
      estimatedEndTime: DATE,
      rejectDescribe: STRING(200),
      actualVisit: STRING(200),
      remark: STRING(200),
      created_time: DATE,
      updated_time: DATE,
    });
    await queryInterface.createTable('y_son_visit_user', {
      id: { type: STRING(36), primaryKey: true },
      idNum: STRING(18),
      name: STRING(50),
      licensePlateNum: STRING(200),
      healthCode: STRING(200),
      tourCode: STRING(200),
      visit_id: STRING,
      created_time: DATE,
      updated_time: DATE,
    });
    await queryInterface.createTable('y_visit_state', {
      id: { type: STRING(36), primaryKey: true },
      statusName: STRING(50),
      createUser_Id: STRING(50),
      describe: STRING(200),
      visit_Id: STRING(50),
      created_time: DATE,
    });
  },
  // 在执行数据库降级时调用的函数，删除 users 表
  down: async queryInterface => {
    await queryInterface.dropTable('y_admin_user');
    await queryInterface.dropTable('y_visit_user');
    await queryInterface.dropTable('y_son_visit_user');
  },
};
